import 'package:flutter/material.dart';
import 'package:wms_plus_action/bloc/inbound/request_list_cubit.dart';
import 'package:wms_plus_action/bloc/outbound/choose_courier_cubit.dart';
import 'package:wms_plus_action/bloc/pick/generate_list_cubit.dart';
import 'package:wms_plus_action/bloc/putaway/pending_to_location_cubit.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:wms_plus_action/repo/out_repo.dart';
import 'package:wms_plus_action/repo/pic_repo.dart';
import 'package:wms_plus_action/repo/put_repo.dart';
import 'package:wms_plus_action/screens/inbound/inb_request_list_page.dart';
import 'package:wms_plus_action/screens/outbound/out_choose_courier_page.dart';
import 'package:wms_plus_action/screens/pick/pic_generate_list_page.dart';
import 'package:wms_plus_action/screens/putaway/put_pending_to_location_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GenActionPage extends StatefulWidget {
  GenActionPage({Key? key}) : super(key: key);

  @override
  _GenActionPageState createState() => _GenActionPageState();
}

class _GenActionPageState extends State<GenActionPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.kWhiteColor,
      body: SafeArea(
        top: true,
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child: Column(
            children: [
              Container(
                  width: MediaQuery.of(context).size.width,
                  margin: const EdgeInsets.only(
                      left: 40.0, top: 40.0),
                  child: Text(
                    StringConstants.action_text_action,
                    style: TextStyle(
                        color: ColorConstants.kPrimaryColor,
                        fontSize: 30,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600),
                  )),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(
                      left: 60.0, top: 40.0, right: 60.0),
                  child: Column(
                    children: <Widget>[

                      // inbound
                      Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(bottom: 35),
                          child: new ElevatedButton(
                            child: Text(
                              StringConstants.action_text_inbound,
                              style: TextStyle(
                                  color: ColorConstants.kWarehouseButtonTextColor,
                                  fontSize: 15,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => RequestListCubit(InboundRepository()),
                                        child: InbRequestListPage(),
                                      )));

                            },
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                              backgroundColor: MaterialStateProperty.all(
                                  ColorConstants.kWarehouseButtonBgColor),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.only(top: 13, bottom: 13)),
                            ),
                          )),

                      // put-away
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: new ElevatedButton(
                            child: Text(
                              StringConstants.action_text_putaway,
                              style: TextStyle(
                                  color: ColorConstants.kWarehouseButtonTextColor,
                                  fontSize: 15,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => PendingToLocationCubit(PutRepository()),
                                        child: PutPendingToLocationPage(),
                                      )));

                            },
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                              backgroundColor: MaterialStateProperty.all(
                                  ColorConstants.kWarehouseButtonBgColor),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.only(top: 13, bottom: 13)),
                            ),
                          )),

                      // picking
                      Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(top: 35),
                          child: new ElevatedButton(
                            child: Text(
                              StringConstants.action_text_picking,
                              style: TextStyle(
                                  color: ColorConstants.kWarehouseButtonTextColor,
                                  fontSize: 15,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => GenerateListCubit(PickRepository()),
                                        child: PicGenerateListPage(),
                                      )));

                            },
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                              backgroundColor: MaterialStateProperty.all(
                                  ColorConstants.kWarehouseButtonBgColor),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.only(top: 13, bottom: 13)),
                            ),
                          )),

                      // outbound
                      Container(
                          width: MediaQuery.of(context).size.width,
                          margin: const EdgeInsets.only(top: 35),
                          child: new ElevatedButton(
                            child: Text(
                              StringConstants.action_text_outbound,
                              style: TextStyle(
                                  color: ColorConstants.kWarehouseButtonTextColor,
                                  fontSize: 15,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w400),
                            ),
                            onPressed: () {

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => BlocProvider(
                                        create: (context) => ChooseCourierCubit(OutboundRepository()),
                                        child: OutChooseCourierPage(),
                                      )));

                            },
                            style: ButtonStyle(
                              shape: MaterialStateProperty.all<
                                  RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                              backgroundColor: MaterialStateProperty.all(
                                  ColorConstants.kWarehouseButtonBgColor),
                              padding: MaterialStateProperty.all(
                                  EdgeInsets.only(top: 13, bottom: 13)),
                            ),
                          )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
