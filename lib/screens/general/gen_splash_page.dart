import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wms_plus_action/bloc/general/login_cubit.dart';
import 'package:wms_plus_action/repo/gen_repo.dart';
import 'package:wms_plus_action/screens/general/gen_login_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GenSplashPage extends StatefulWidget {
  @override
  _GenSplashPageState createState() => _GenSplashPageState();
}

class _GenSplashPageState extends State<GenSplashPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    _clearSharedPreference();

    Timer(Duration(seconds: 3), () {
      Navigator.push(context, MaterialPageRoute(builder: (context) => BlocProvider(
          create: (context) => LoginCubit(GeneralRepository()),
          child: GenLoginPage(
              versionName: _packageInfo.version,
              versionCode: _packageInfo.buildNumber),
        )
      ));
    });
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Future<void> _clearSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    prefs.remove('tokenExp');
    prefs.remove('loginFlag');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.kThemeColor,
    );
  }
}
