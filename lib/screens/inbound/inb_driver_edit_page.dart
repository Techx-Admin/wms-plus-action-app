import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wms_plus_action/bloc/inbound/driver_edit_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/model/inbound/driveredit/create_inb_driver_info_model.dart';
import 'package:wms_plus_action/model/inbound/driveredit/update_inb_driver_info_model.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';

class InbDriverEditPage extends StatefulWidget {
  String requestDeliveryId,
      requestId,
      driverName,
      driverIcNo,
      driverContact,
      carPlateNo,
      carParkAt,
      status,
      typeOfData;

  InbDriverEditPage(
      {Key? key,
      required this.requestDeliveryId,
      required this.requestId,
      required this.driverName,
      required this.driverIcNo,
      required this.driverContact,
      required this.carPlateNo,
      required this.carParkAt,
      required this.status,
      required this.typeOfData})
      : super(key: key);

  @override
  _InbDriverEditPageState createState() => _InbDriverEditPageState();
}

class _InbDriverEditPageState extends State<InbDriverEditPage> {
  late DriverEditCubit cubit;
  late ProgressDialog pr;
  TextEditingController textDriverName = TextEditingController();
  TextEditingController textDriverIc = TextEditingController();
  TextEditingController textVehiclePlate = TextEditingController();
  TextEditingController textBayStop = TextEditingController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      if (widget.typeOfData == "edit") {
        textDriverName.text = widget.driverName;
        textDriverIc.text = widget.driverIcNo;
        textVehiclePlate.text = widget.carPlateNo;
        textBayStop.text = widget.carParkAt;
      }
    });
  }

  checkTextFieldEmptyOrNot(BuildContext context, DriverEditCubit cubit) {
    String newDriverName = textDriverName.text;
    String newDriverIcNo = textDriverIc.text;
    String newCarPlateNo = textVehiclePlate.text;
    String newCarParkAt = textBayStop.text;

    if (newDriverName == '') {
      showToast("Please Enter your driver name", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      if (newDriverIcNo == '') {
        showToast("Please Enter your driver IC", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        if (newCarPlateNo == '') {
          showToast("Please Enter your vehicle plate number", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
        else {
          if (newCarParkAt == '') {
            showToast("Please Enter your bay driver stop & unload", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
          else {
            if (widget.typeOfData == "edit") {
              if (widget.driverName == newDriverName &&
                  widget.driverIcNo == newDriverIcNo &&
                  widget.carPlateNo == newCarPlateNo &&
                  widget.carParkAt == newCarParkAt) {
                showToast("No changes", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
              else {
                _checkInternetConnection(newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt, cubit);
              }
            }
            else {
              _checkInternetConnection(newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt, cubit);
            }
          }
        }
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future<void> _checkInternetConnection(newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt, DriverEditCubit cubit) async {
    try {
      final response = await InternetAddress.lookup('www.google.com');
      if (response.isNotEmpty) {
        sendData(newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt, cubit);
      }
    } on SocketException catch (err) {
      showToast(StringConstants.util_alert_no_connection_msg,
          Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
  }

  Future<void> sendData(newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt, DriverEditCubit cubit) async {
    await pr.show();

    if (widget.typeOfData == "edit") {

      final response = await cubit.updateInboundDelivery(
          widget.requestId, widget.requestDeliveryId,
          newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt);
      String responseInString = await response;


      if (responseInString == "") {
        await pr.hide();

        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {

        var res = json.decode(responseInString);
        UpdateInbDriverInfo _res = UpdateInbDriverInfo.fromJson(res);
        if(_res.ret == '0') {
          if(_res.data.updatedRow == 1) {
            await pr.hide();

            showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

            Navigator.pop(context, widget.requestId);
          }
          else{
            await pr.hide();

            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
        else {
          await pr.hide();

          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
      }
    }
    else {

      final response = await cubit.createInboundDelivery(
          widget.requestId, newDriverName, newDriverIcNo, newCarPlateNo, newCarParkAt);
      String responseInString = await response;

      if (responseInString == "") {
        await pr.hide();

        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        var res = json.decode(responseInString);
        CreateInbDriverInfo _res = CreateInbDriverInfo.fromJson(res);
        if (_res.ret == "0") {
          await pr.hide();

          showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

          Navigator.pop(context, widget.requestId);
        } else {
          await pr.hide();

          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
      }

    }
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    cubit = context.watch<DriverEditCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {},
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 120,

                        child: Column(
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(
                                    left: 60.0, right: 60.0, bottom: 10.0),
                                child: new ElevatedButton(
                                  child: Text(
                                    'Submit',
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kGenPicklistButtonTextColor,
                                        fontSize: 15,
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w600),
                                  ),
                                  onPressed: () {
                                    checkTextFieldEmptyOrNot(context, cubit);
                                  },
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    )),
                                    backgroundColor: MaterialStateProperty.all(
                                        ColorConstants.kThemeColor),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.only(top: 10, bottom: 10)),
                                  ),
                                )),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: const EdgeInsets.only(bottom: 150.0),
                    child: SingleChildScrollView(
                      child: Card(
                        margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          children: <Widget>[

                            // page header
                            Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                height: 50,
                                child: Center(
                                  child: Text(
                                    'Driver',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color:
                                            ColorConstants.kAssignTrayHeaderColor,
                                        fontSize: 17,
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),

                            // driver name
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
                              child: Text(
                                'Driver Name',
                                style: TextStyle(
                                    color: ColorConstants.kInbDriverLabelTextColor,
                                    fontSize: 14,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
                                child: TextField(
                                  controller: textDriverName,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.sentences,
                                )
                            ),

                            // driver ic
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                              child: Text(
                                'Driver IC',
                                style: TextStyle(
                                    color: ColorConstants.kInbDriverLabelTextColor,
                                    fontSize: 14,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
                                child: TextField(
                                  controller: textDriverIc,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                )
                            ),

                            // vehicle plate no
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                              child: Text(
                                'Vehicle Plate Number',
                                style: TextStyle(
                                    color: ColorConstants.kInbDriverLabelTextColor,
                                    fontSize: 14,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0),
                                child: TextField(
                                  controller: textVehiclePlate,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.characters,
                                )
                            ),

                            // bay driver stop
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0),
                              child: Text(
                                'Bay Driver Stop & Unload',
                                style: TextStyle(
                                    color: ColorConstants.kInbDriverLabelTextColor,
                                    fontSize: 14,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 30.0),
                                child: TextField(
                                  controller: textBayStop,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.done,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.digitsOnly
                                  ],
                                )
                            ),

                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
