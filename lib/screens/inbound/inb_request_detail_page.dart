import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:wms_plus_action/bloc/inbound/driver_list_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/request_detail_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/bloc/inbound/submit_batch_qty_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/submit_dispute_cubit.dart';
import 'package:wms_plus_action/model/inbound/requestdetail/get_inb_request_detail_model.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'inb_driver_list_page.dart';
import 'inb_submit_batch_qty_page.dart';
import 'inb_submit_dispute_page.dart';

class InbRequestDetailPage extends StatefulWidget {
  String inboundId, inboundNo, totalQty;

  InbRequestDetailPage(
      {Key? key,
      required this.inboundId,
      required this.inboundNo,
      required this.totalQty})
      : super(key: key);

  @override
  _InbRequestDetailPageState createState() => _InbRequestDetailPageState();
}

class _InbRequestDetailPageState extends State<InbRequestDetailPage> {
  late RequestDetailCubit cubit;
  late bool isVisibleEnableBtn;
  late bool isVisibleDisableBtn;
  TextEditingController textTotalAndInboundQty = TextEditingController();
  List<String> _listItemId        = [];
  List<String> _listSkuId         = [];
  List<String> _listSkuCode       = [];
  List<String> _listTotalQty      = [];
  List<String> _listInboundQty    = [];
  List<String> _listDisputeQty    = [];
  List<String> _listStatusInString= [];
  List<String> _listBarcode       = [];
  bool completeInboundFlag = false;

  @override
  void initState() {
    super.initState();

    isVisibleEnableBtn = false;
    isVisibleDisableBtn = true;

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getInboundRequestDetail(widget.inboundId);
    });
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<RequestDetailCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataReqDetailState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            int inboundTotal = 0;
            completeInboundFlag = true;

            var res = json.decode(responseInString);
            GetInbRequestDetail _res = GetInbRequestDetail.fromJson(res);
            GetInbRequestDetailData _resData = GetInbRequestDetailData.fromJson(_res.data.toJson());

            for (int i = 0; i < _resData.inboundRequestItem.length; i++) {
              int requestItemId = _resData.inboundRequestItem[i].id;
              String skuId = _resData.inboundRequestItem[i].skuId.toString();
              int itemDetailQty = _resData.inboundRequestItem[i].qty;
              int disputeDetailQty =  _resData.inboundRequestItem[i].qty;
              int status = _resData.inboundRequestItem[i].status;
              String barcode1 = _resData.inboundRequestItem[i].sku.barcode1;

              String skuCode = "-";
              try {
                if(_resData.inboundRequestItem[i].sku != null)
                  skuCode = _resData.inboundRequestItem[i].sku.skuCode;
              } on Exception catch (e) {
                print(e.toString());
              }

              int? inboundDetailQty = 0;

              try {
                inboundDetailQty = _resData.inboundRequestItem[i].sku.skuBatch?.qty;
              } on Exception catch (e) {
                print(e.toString());
              }

              if(inboundDetailQty == null) {
                inboundDetailQty = 0;
              }

              _listItemId.add(requestItemId.toString());
              _listSkuId.add(skuId);
              _listSkuCode.add(skuCode);
              _listTotalQty.add(itemDetailQty.toString());
              _listInboundQty.add(inboundDetailQty.toString());
              _listDisputeQty.add(disputeDetailQty.toString());
              _listStatusInString.add(status.toString());

              if(barcode1 != "") {
                _listBarcode.add(barcode1);
              }
              else{
                _listBarcode.add("");
              }

              inboundTotal = inboundTotal + inboundDetailQty!;
            }

            textTotalAndInboundQty.text = inboundTotal.toString() + " / " + widget.totalQty;

            setState(() {

            });
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 110,

                        child: Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                      margin: const EdgeInsets.only(
                                          left: 20.0, right: 10.0),
                                      child: new ElevatedButton(
                                        child: Text(
                                          'Driver',
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistButtonTextColor,
                                              fontSize: 15,
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                        onPressed: () {

                                          Navigator.push(context, MaterialPageRoute(builder: (context) => BlocProvider(
                                            create: (context) => DriverListCubit(InboundRepository()),
                                            child: InbDriverListPage(inboundId: widget.inboundId),
                                          )
                                          ));

                                        },
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          )),
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  ColorConstants.kThemeColor),
                                          padding: MaterialStateProperty.all(
                                              EdgeInsets.only(
                                                  top: 10, bottom: 10)),
                                        ),
                                      )),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 20.0),
                                      child: Column(
                                        children: [
                                          Visibility(
                                            visible: isVisibleEnableBtn,
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: new ElevatedButton(
                                                child: Text(
                                                  'Next',
                                                  style: TextStyle(
                                                      color: ColorConstants
                                                          .kGenPicklistButtonTextColor,
                                                      fontSize: 15,
                                                      fontFamily: 'Montserrat',
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                onPressed: () {
                                                  // dispute page
                                                  bool allDone = true;

                                                  int size = _listItemId.length;
                                                    for(int i = 0 ; i < size ; i++){
                                                    if(_listStatusInString[i] == "1"){

                                                    }
                                                    else {
                                                      allDone = false;
                                                      break;
                                                    }
                                                  }

                                                  if(allDone) {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) => BlocProvider(
                                                              create: (context) =>
                                                                  SubmitDisputeCubit(InboundRepository()),
                                                              child: InbSubmitDisputePage(
                                                                  inboundId: widget.inboundId),
                                                            )));

                                                  }
                                                  else {
                                                    showToast("Please complete Inbound", Toast.LENGTH_SHORT,
                                                        ToastGravity.BOTTOM);
                                                  }
                                                },
                                                style: ButtonStyle(
                                                  shape: MaterialStateProperty
                                                      .all<RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25.0),
                                                  )),
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          ColorConstants
                                                              .kThemeColor),
                                                  padding:
                                                      MaterialStateProperty.all(
                                                          EdgeInsets.only(
                                                              top: 10,
                                                              bottom: 10)),
                                                ),
                                              ),
                                            ),
                                          ),
                                          Visibility(
                                            visible: isVisibleDisableBtn,
                                            child: Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              child: new ElevatedButton(
                                                child: Text(
                                                  'Next',
                                                  style: TextStyle(
                                                      color: ColorConstants
                                                          .kButtonTextColor,
                                                      fontSize: 15,
                                                      fontFamily: 'Montserrat',
                                                      fontWeight:
                                                          FontWeight.w600),
                                                ),
                                                onPressed: () {
                                                  // dispute page
                                                  bool allDone = true;

                                                  int size = _listItemId.length;
                                                  for(int i = 0 ; i < size ; i++){
                                                    if(_listStatusInString[i] == "1"){

                                                    }
                                                    else {
                                                      allDone = false;
                                                      break;
                                                    }
                                                  }

                                                  if(allDone) {
                                                    Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (context) => BlocProvider(
                                                              create: (context) =>
                                                                  SubmitDisputeCubit(InboundRepository()),
                                                              child: InbSubmitDisputePage(
                                                                  inboundId: widget.inboundId),
                                                            )));

                                                  }
                                                  else {
                                                    showToast("Please complete Inbound", Toast.LENGTH_SHORT,
                                                        ToastGravity.BOTTOM);
                                                  }
                                                },
                                                style: ButtonStyle(
                                                  shape: MaterialStateProperty
                                                      .all<RoundedRectangleBorder>(
                                                          RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            25.0),
                                                  )),
                                                  backgroundColor:
                                                      MaterialStateProperty.all(
                                                          ColorConstants
                                                              .kButtonBgColor),
                                                  padding:
                                                      MaterialStateProperty.all(
                                                          EdgeInsets.only(
                                                              top: 10,
                                                              bottom: 10)),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      )),
                                ),
                              ],
                            ),
                            Visibility(
                              visible: false,
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.only(left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {},
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                      )),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Visibility(
                              visible: false,
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {},
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                              RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(25.0),
                                      )),
                                      backgroundColor:
                                          MaterialStateProperty.all(
                                              ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Ink(
                                      height: 60,
                                      padding: const EdgeInsets.all(5.0),
                                      decoration: ShapeDecoration(
                                        color: ColorConstants.kThemeColor,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Image.asset(
                                          'images/icon_topbar_scan.png',
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          scanQr();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_menu.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {},
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ],
                        ),
                      )),

                  // top
                  Column(
                    children: [
                      Container(
                        child: Wrap(children: [
                          Card(
                            margin: const EdgeInsets.only(
                                bottom: 100.0,
                                top: 15.0,
                                left: 20.0,
                                right: 20.0),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                            elevation: 10,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 15,
                                      child: Container(
                                        child: Image.asset(
                                          "images/icon_inbound_box.png",
                                          fit: BoxFit.none,
                                          width: 90,
                                          height: 90,
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 25,
                                      child: Container(
                                        child: Column(
                                          children: [
                                            Container(
                                              child: TextField(
                                                controller:
                                                    textTotalAndInboundQty,
                                                enabled: false,
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: ColorConstants.kGenPicklistTextFieldColor,
                                                    fontSize: 20,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w600),
                                                // decoration: InputDecoration(border: OutlineInputBorder()),
                                                decoration: new InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                            left: 0,
                                                            bottom: 0,
                                                            top: 0,
                                                            right: 0),
                                                    border: InputBorder.none,
                                                    focusedBorder: InputBorder.none,
                                                    enabledBorder: InputBorder.none,
                                                    errorBorder: InputBorder.none,
                                                    disabledBorder: InputBorder.none),
                                              ),
                                            ),
                                            Container(
                                              width: MediaQuery.of(context).size.width,
                                              child: Text(
                                                'Total Quantity To Be Inbound',
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                    color: ColorConstants
                                                        .kInbDetailGreyColor,
                                                    fontSize: 12,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ],
                            ),
                          )
                        ]),
                      ),
                    ],
                  ),

                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Card(
                      margin: const EdgeInsets.only(
                          bottom: 150.0, top: 120.0, left: 20.0, right: 20.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kPendingLocationTopBgColor,
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  'Inbound',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color:
                                          ColorConstants.kAssignTrayHeaderColor,
                                      fontSize: 17,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )),

                          // listview
                          Expanded(
                            child: Container(
                              child: ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: _listItemId.length,
                                  separatorBuilder:
                                      (BuildContext context, int index) =>
                                      Divider(
                                          height: 1,
                                          color: ColorConstants.kAssignTrayLineColor),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: [
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 20,
                                              child: Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  color: changeListview(index),
                                                  padding: const EdgeInsets.only(left: 20.0,top: 10.0, bottom: 10.0),
                                                  child: Text(
                                                    _listBarcode[index],
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 12,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight:
                                                        FontWeight.w400),
                                                  )
                                              ),
                                            ),
                                            Expanded(
                                              flex: 10,
                                              child: Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  color: changeListview(index),
                                                  padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                                                  child: Text(
                                                    _listTotalQty[index],
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 12,
                                                        fontFamily:
                                                        'Montserrat',
                                                        fontWeight:
                                                        FontWeight.w400),
                                                  )),
                                            ),
                                            Expanded(
                                              flex: 10,
                                              child: Container(
                                                  width: MediaQuery.of(context).size.width,
                                                  color: changeListview(index),
                                                  padding: const EdgeInsets.only(right: 20.0,top: 10.0, bottom: 10.0),
                                                  child: Text(
                                                    _listInboundQty[index],
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 12,
                                                        fontFamily:
                                                        'Montserrat',
                                                        fontWeight:
                                                        FontWeight.w400),
                                                  )),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scanQr() async {
    String barcode = "-1";
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcode = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
    } on Exception {

    }
    if (barcode == "-1") {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {

      String totalQty = "";
      String requestItemId = "";
      String skuId = "";
      String skuCode = "";
      bool result = false;

      int size = _listItemId.length;

      for(int i = 0 ; i < size ; i++){
        if(barcode == _listBarcode[i]){
          totalQty = _listTotalQty[i];
          skuId = _listSkuId[i];
          skuCode = _listSkuCode[i];
          requestItemId = _listItemId[i];
          result = true;
          break;
        }
      }

      if(result){
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString('requestItemId', requestItemId);

        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => BlocProvider(
                  create: (context) =>
                      SubmitBatchQtyCubit(InboundRepository()),
                  child: InbSubmitBatchQtyPage(
                      skuId: skuId,
                      skuCode: skuCode,
                      barcode: barcode,
                      totalQty: totalQty),
                )));
      }
      else {
        showToast("Sku Code not found", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  changeListview(int index){
    if(_listStatusInString[index] == "1"){
      return ColorConstants.kInbRequestItemDone;
    }
    else {
      return null;
    }
  }
}
