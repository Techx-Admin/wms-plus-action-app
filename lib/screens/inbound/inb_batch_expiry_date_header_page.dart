import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wms_plus_action/bloc/inbound/batch_expiry_date_detail_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/batch_expiry_date_header_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/model/inbound/batchcontainer/get_inb_batch_location_model.dart';
import 'package:wms_plus_action/model/inbound/batchexpirydateheader/body_inb_sku_location_bulk_model.dart';
import 'package:wms_plus_action/model/inbound/batchexpirydateheader/create_inb_sku_location_bulk_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/get_inb_sku_location_model.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:wms_plus_action/screens/inbound/inb_batch_expiry_date_detail_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:need_resume/need_resume.dart';

class InbBatchExpiryDateHeaderPage extends StatefulWidget {
  @override
  _InbBatchExpiryDateHeaderPageState createState() =>
      _InbBatchExpiryDateHeaderPageState();
}

class _InbBatchExpiryDateHeaderPageState
    extends ResumableState<InbBatchExpiryDateHeaderPage> {
  late BatchExpiryDateHeaderCubit cubit;
  List<String> _listItemId = [];
  List<String> _listExpiryDate = [];
  List<String> _listQuantity = [];
  List<bool> _listAddNew = [];
  List<bool> _listCompleteBatch = [];

  String sharedBarcode = "", sharedTotalQty = ""; // total qty is inbound qty
  List<TextEditingController> textListExpiryDate = [];
  List<TextEditingController> textListQty = [];
  int doneUpdate = 0;
  bool isAddNewButton = false;
  bool isNotNewButton = false;

  @override
  void onResume() {
    switch (resume.source) {
      case 'expiry_date_screen':
        updateList(resume.data.toString());
        break;
    }
  }

  updateList(String itemId){
    int size = _listItemId.length;

    for (int i = 0; i < size; i++) {
      if(_listItemId[i] == itemId) {
        _listAddNew[i] = false;
        _listCompleteBatch[i] = true;

        break;
      }
    }

    setState(() {

    });
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sharedBarcode = prefs.getString('barcode').toString();
    sharedTotalQty = prefs.getString('inboundQty').toString();

    cubit.getSkuLocationId();
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<BatchExpiryDateHeaderCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataDateHeaderState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            if (responseInString == "") {
              isAddNewButton = true;
              isNotNewButton = false;

              setState(() {

              });
            }
            else {
              var res = json.decode(responseInString);
              GetInbSkuLocation _res = GetInbSkuLocation.fromJson(res);

              if (_res.ret == "0") {
                int totalRecord = _res.data.totalCount;
                if (totalRecord > 0) {
                  int skuSize = _res.data.records.length;

                  for (int i = 0; i < skuSize; i++) {
                    if (_res.data.records[i].expiryDate == null) {

                    }
                    else {
                      String skuLocationId = _res.data.records[i].id.toString();
                      String expiryDate = _res.data.records[i].expiryDate.replaceAll("Z", " ")
                          .replaceAll("T", " ").substring(0,10);
                      String batchQty = _res.data.records[i].qty.toString();

                      _listItemId.add(skuLocationId);
                      _listExpiryDate.add(expiryDate);
                      _listQuantity.add(batchQty);
                      _listAddNew.add(false);
                      _listCompleteBatch.add(false);
                    }
                  }

                  if(_listItemId.length > 0) {
                    isAddNewButton = false;
                    isNotNewButton = true;
                    doneUpdate = 1;

                    checkBatchLocationData();
                  }
                  else {
                    isAddNewButton = true;
                    isNotNewButton = false;

                    setState(() {

                    });
                  }
                }
              }
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Column(
                        children: [

                          // with button
                          Visibility(
                            visible: isAddNewButton,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 120,

                              child: Column(
                                children: [
                                  Container(
                                      width: MediaQuery.of(context).size.width,
                                      margin: const EdgeInsets.only(
                                          left: 60.0, right: 60.0, bottom: 10.0),
                                      child: new ElevatedButton(
                                        child: Text(
                                          'Submit',
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistButtonTextColor,
                                              fontSize: 15,
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                        onPressed: () {
                                          checkTextFieldEmptyOrNot1();
                                        },
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          )),
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  ColorConstants.kThemeColor),
                                          padding: MaterialStateProperty.all(
                                              EdgeInsets.only(top: 10, bottom: 10)),
                                        ),
                                      )),
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: IconButton(
                                            iconSize: 20,
                                            padding: const EdgeInsets.all(15.0),
                                            icon: Image.asset(
                                              'images/icon_topbar_back.png',
                                              color: Colors.black,
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop(true);
                                              // Navigator.pop(context, widget.requestId);
                                            },
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),

                          // without button
                          Visibility(
                            visible: isNotNewButton,
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 60,

                              child: Column(
                                children: [
                                  Row(
                                    children: <Widget>[
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                          child: IconButton(
                                            iconSize: 20,
                                            padding: const EdgeInsets.all(15.0),
                                            icon: Image.asset(
                                              'images/icon_topbar_back.png',
                                              color: Colors.black,
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop(true);
                                            },
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: Container(),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: EdgeInsets.only(
                        bottom: showMarginBottom()
                    ),
                    child: SingleChildScrollView(
                      child: Column(
                        children: <Widget>[

                          // record
                          ListView.builder(
                              shrinkWrap: true,
                              itemCount: _listItemId.length,
                              physics: NeverScrollableScrollPhysics(),
                              itemBuilder: (BuildContext context, int index) {
                                textListExpiryDate.add(new TextEditingController());
                                textListQty.add(new TextEditingController());

                                return Column(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Card(
                                        margin: const EdgeInsets.only(
                                            top: 20.0,
                                            left: 20.0,
                                            right: 20.0,
                                            bottom: 10.0),
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(20.0),
                                        ),
                                        elevation: 10,
                                        child: Wrap(
                                          children: <Widget>[
                                            // listview header
                                            Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.only(
                                                      topLeft:
                                                          Radius.circular(10.0),
                                                      topRight:
                                                          Radius.circular(10.0)),
                                                  color: ColorConstants
                                                      .kAssignTrayTopBgColor,
                                                ),
                                                child: Column(
                                                  children: [
                                                    // show barcode
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      margin:
                                                          const EdgeInsets.only(
                                                              left: 10.0,
                                                              right: 10.0,
                                                              top: 10.0),
                                                      child: Text(
                                                        sharedBarcode,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: ColorConstants
                                                                .kInbListAdapterTextColor,
                                                            fontSize: 17,
                                                            fontFamily:
                                                                'Montserrat',
                                                            fontWeight:
                                                                FontWeight.w400),
                                                      ),
                                                    ),

                                                    // show qty
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      margin:
                                                          const EdgeInsets.only(
                                                        left: 10.0,
                                                        right: 10.0,
                                                        top: 5.0,
                                                        bottom: 15.0,
                                                      ),

                                                      child: RichText(
                                                        textAlign:
                                                            TextAlign.center,
                                                        text: TextSpan(
                                                            children: <TextSpan>[
                                                              TextSpan(
                                                                  text:
                                                                      "Quantity : ",
                                                                  style: TextStyle(
                                                                      color: ColorConstants
                                                                          .kInbBatchQtyTextColor,
                                                                      fontFamily:
                                                                          'Montserrat',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400)),
                                                              TextSpan(
                                                                  text:
                                                                      sharedTotalQty,
                                                                  style: TextStyle(
                                                                      color: ColorConstants
                                                                          .kInbRedColor,
                                                                      fontFamily:
                                                                          'Montserrat',
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w400)),
                                                            ]),
                                                      ),
                                                    ),
                                                  ],
                                                )),

                                            // expiry date
                                            Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: const EdgeInsets.only(
                                                  left: 20.0,
                                                  right: 20.0,
                                                  top: 30.0),
                                              child: Text(
                                                'Expiry Date',
                                                style: TextStyle(
                                                    color: ColorConstants
                                                        .kInbDriverLabelTextColor,
                                                    fontSize: 14,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ),
                                            Visibility(
                                              visible: isAddNewButton,
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 20.0,
                                                      right: 20.0,
                                                      top: 5.0),
                                                  child: TextField(
                                                    controller: textListExpiryDate[index],
                                                    readOnly: true,
                                                    onTap: () async {
                                                      DateTime? pickedDate =
                                                      await showDatePicker(
                                                          context: context,
                                                          initialDate:
                                                          DateTime.now(),
                                                          firstDate:
                                                          DateTime(2000),
                                                          //DateTime.now() - not to allow to choose before today.
                                                          lastDate:
                                                          DateTime(2101));

                                                      if (pickedDate != null) {
                                                        String formattedDate =
                                                        DateFormat(
                                                            'yyyy-MM-dd')
                                                            .format(pickedDate);

                                                        setState(() {
                                                          textListExpiryDate[index]
                                                              .text =
                                                              formattedDate; //set output date to TextField value.
                                                        });
                                                      } else {

                                                      }
                                                    },
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kPrimaryColor,
                                                        fontSize: 16,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                    decoration: InputDecoration(
                                                      border: OutlineInputBorder(),
                                                    ),
                                                    textInputAction:
                                                        TextInputAction.next,
                                                  )),
                                            ),
                                            Visibility(
                                              visible: false,
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 20.0,
                                                      right: 20.0,
                                                      top: 5.0),
                                                  child: TextField(
                                                    controller: textListExpiryDate[index],
                                                    enabled: false,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kPrimaryColor,
                                                        fontSize: 16,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight:
                                                        FontWeight.w400),
                                                    decoration: InputDecoration(
                                                      border: OutlineInputBorder(),
                                                    ),
                                                    textInputAction:
                                                    TextInputAction.next,
                                                  )),
                                            ),
                                            Visibility(
                                              visible: isNotNewButton,
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                margin: const EdgeInsets.only(
                                                    left: 20.0,
                                                    right: 20.0,
                                                    top: 5.0),
                                                child: Text(
                                                  _listExpiryDate[index],
                                                  style: TextStyle(
                                                      color: ColorConstants
                                                          .kPrimaryColor,
                                                      fontSize: 16,
                                                      fontFamily: 'Montserrat',
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ),
                                            ),

                                            // qty
                                            Container(
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              margin: const EdgeInsets.only(
                                                  left: 20.0,
                                                  right: 20.0,
                                                  top: 20.0),
                                              child: Text(
                                                'Enter Inbound Quantity',
                                                style: TextStyle(
                                                    color: ColorConstants
                                                        .kInbDriverLabelTextColor,
                                                    fontSize: 14,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ),
                                            Visibility(
                                              visible: isAddNewButton,
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 20.0,
                                                      right: 20.0,
                                                      top: 5.0,
                                                      bottom: 10.0),
                                                  child: TextField(
                                                    controller: textListQty[index],
                                                    enabled: true,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kPrimaryColor,
                                                        fontSize: 16,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                    decoration: InputDecoration(
                                                      border: OutlineInputBorder(),
                                                    ),
                                                    textInputAction:
                                                        TextInputAction.done,
                                                    keyboardType:
                                                        TextInputType.number,
                                                    inputFormatters: <
                                                        TextInputFormatter>[
                                                      FilteringTextInputFormatter
                                                          .digitsOnly
                                                    ],
                                                  )),
                                            ),
                                            Visibility(
                                              visible: false,
                                              child: Container(
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 20.0,
                                                      right: 20.0,
                                                      top: 5.0,
                                                      bottom: 10.0),
                                                  child: TextField(
                                                    controller: textListQty[index],
                                                    enabled: false,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kPrimaryColor,
                                                        fontSize: 16,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight:
                                                        FontWeight.w400),
                                                    decoration: InputDecoration(
                                                      border: OutlineInputBorder(),
                                                    ),
                                                    textInputAction:
                                                    TextInputAction.done,
                                                    keyboardType:
                                                    TextInputType.number,
                                                    inputFormatters: <
                                                        TextInputFormatter>[
                                                      FilteringTextInputFormatter
                                                          .digitsOnly
                                                    ],
                                                  )),
                                            ),
                                            Visibility(
                                              visible: isNotNewButton,
                                              child: Container(
                                                width: MediaQuery.of(context)
                                                    .size
                                                    .width,
                                                margin: const EdgeInsets.only(
                                                    left: 20.0,
                                                    right: 20.0,
                                                    top: 5.0),
                                                child: Text(
                                                  _listQuantity[index],
                                                  style: TextStyle(
                                                      color: ColorConstants
                                                          .kPrimaryColor,
                                                      fontSize: 16,
                                                      fontFamily: 'Montserrat',
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ),
                                            ),

                                            // add container
                                            showAddContainer(index),
                                            Visibility(
                                              visible: false,
                                              child: Container(
                                                margin: const EdgeInsets.only(
                                                    left: 20.0,
                                                    right: 20.0,
                                                    bottom: 10.0),
                                                child: GestureDetector(
                                                  onTap: () {

                                                  },
                                                  child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    child: Card(
                                                      shape: RoundedRectangleBorder(
                                                        borderRadius: BorderRadius.circular(20.0),
                                                      ),
                                                      elevation: 10,
                                                      child: Wrap(
                                                        children: <Widget>[
                                                          Container(
                                                              width: MediaQuery.of(context).size.width,
                                                              decoration: BoxDecoration(
                                                                borderRadius: BorderRadius.only(
                                                                    bottomLeft: Radius.circular(10.0),
                                                                    bottomRight: Radius.circular(10.0),
                                                                    topLeft: Radius.circular(10.0),
                                                                    topRight: Radius.circular(10.0)),
                                                              ),
                                                              child: Center(
                                                                child: Wrap(
                                                                  children: [
                                                                    // add icon
                                                                    Container(
                                                                      height: 70.0,
                                                                      child: Ink(
                                                                        width: 50,
                                                                        height: 50,
                                                                        padding:
                                                                        const EdgeInsets.all(5.0),
                                                                        decoration: ShapeDecoration(
                                                                          color:
                                                                          ColorConstants.kThemeColor,
                                                                          shape: CircleBorder(),
                                                                        ),
                                                                        child: Center(
                                                                          child: Text(
                                                                            '+',
                                                                            style: TextStyle(
                                                                                color: ColorConstants
                                                                                    .kWhiteColor,
                                                                                fontSize: 24,
                                                                                fontFamily: 'Montserrat',
                                                                                fontWeight:
                                                                                FontWeight.w600),
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),

                                                                    // add expiry date
                                                                    Container(
                                                                      width: 150.0,
                                                                      height: 70.0,
                                                                      margin: const EdgeInsets.only(
                                                                          left: 10.0),
                                                                      child: Center(
                                                                        child: Row(
                                                                          mainAxisAlignment:
                                                                          MainAxisAlignment.center,
                                                                          children: <Widget>[
                                                                            Text(
                                                                              'Add Container',
                                                                              style: TextStyle(
                                                                                  color: ColorConstants
                                                                                      .kInbListAdapterTextColor,
                                                                                  fontSize: 14,
                                                                                  fontFamily:
                                                                                  'Montserrat',
                                                                                  fontWeight:
                                                                                  FontWeight.w400),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                );
                              }),

                          // add new
                          Visibility(
                            visible: isAddNewButton,
                            child: Container(
                              child: GestureDetector(
                                onTap: () {
                                  _listItemId.add("");
                                  _listExpiryDate.add("");
                                  _listQuantity.add("");
                                  _listAddNew.add(true);
                                  _listCompleteBatch.add(false);

                                  setState(() {

                                  });
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  child: Card(
                                    margin: const EdgeInsets.only(
                                        top: 20.0,
                                        left: 20.0,
                                        right: 20.0,
                                        bottom: 10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    elevation: 10,
                                    child: Wrap(
                                      children: <Widget>[
                                        Container(
                                            width: MediaQuery.of(context).size.width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  bottomLeft: Radius.circular(10.0),
                                                  bottomRight: Radius.circular(10.0),
                                                  topLeft: Radius.circular(10.0),
                                                  topRight: Radius.circular(10.0)),
                                            ),
                                            child: Center(
                                              child: Wrap(
                                                children: [
                                                  // add icon
                                                  Container(
                                                    height: 70.0,
                                                    child: Ink(
                                                      width: 50,
                                                      height: 50,
                                                      padding:
                                                          const EdgeInsets.all(5.0),
                                                      decoration: ShapeDecoration(
                                                        color:
                                                            ColorConstants.kThemeColor,
                                                        shape: CircleBorder(),
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                          '+',
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kWhiteColor,
                                                              fontSize: 24,
                                                              fontFamily: 'Montserrat',
                                                              fontWeight:
                                                                  FontWeight.w600),
                                                        ),
                                                      ),
                                                    ),
                                                  ),

                                                  // add expiry date
                                                  Container(
                                                    width: 150.0,
                                                    height: 70.0,
                                                    margin: const EdgeInsets.only(
                                                        left: 10.0),
                                                    child: Center(
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment.center,
                                                        children: <Widget>[
                                                          Text(
                                                            'Add New Expiry Date',
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                    'Montserrat',
                                                                fontWeight:
                                                                    FontWeight.w400),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  showMarginBottom() {
    // 1 - only see 100
    // 0 - have submit button 150
    if(doneUpdate == 1)
      return 100.0;
    else
      return 150.0;
  }

  showAddContainer(int index) {
    if(! _listAddNew[index]) {
      return
        Container(
          child: GestureDetector(
            onTap: () {

              int size = _listItemId.length;
              int incompleteBatch = 0;

              if (size > 0) {
                for (int i = 0; i < size; i++) {
                  if(! _listCompleteBatch[i]){
                    incompleteBatch = incompleteBatch + 1;
                  }
                }
              }

              push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) => BatchExpiryDateDetailCubit(InboundRepository()),
                        child: InbBatchExpiryDateDetailPage(
                            keepItemId: _listItemId[index],
                            keepExpiryDate: _listExpiryDate[index],
                            keepQuantity: _listQuantity[index],
                            keepIncompleteLeft: incompleteBatch,
                        ),
                      )), 'expiry_date_screen'
              );
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Card(
                margin: const EdgeInsets.only(
                    top: 20.0,
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10.0),
                    bottomRight: Radius.circular(10.0),
                  ),
                ),
                elevation: 10,
                child: Wrap(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10.0),
                              bottomRight: Radius.circular(10.0),
                              topLeft: Radius.circular(10.0),
                              topRight: Radius.circular(10.0)),
                        ),
                        child: Center(
                          child: Wrap(
                            children: [
                              // add icon
                              Container(
                                height: 70.0,
                                child: Ink(
                                  width: 50,
                                  height: 50,
                                  padding:
                                  const EdgeInsets.all(5.0),
                                  decoration: ShapeDecoration(
                                    color:
                                    ColorConstants.kThemeColor,
                                    shape: CircleBorder(),
                                  ),
                                  child: Center(
                                    child: Text(
                                      '+',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kWhiteColor,
                                          fontSize: 24,
                                          fontFamily: 'Montserrat',
                                          fontWeight:
                                          FontWeight.w600),
                                    ),
                                  ),
                                ),
                              ),

                              // add expiry date
                              Container(
                                width: 150.0,
                                height: 70.0,
                                margin: const EdgeInsets.only(
                                    left: 10.0),
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        'Add New Container',
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 14,
                                            fontFamily:
                                            'Montserrat',
                                            fontWeight:
                                            FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )),
                  ],
                ),
              ),
            ),
          ),
        );
    }
    else {
      return
        Container();
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future<void> checkTextFieldEmptyOrNot1() async{
    if(doneUpdate == 0) {
      int size = _listItemId.length;

      if (size > 0) {
        bool listEmptyDate = false;
        bool listIsInPending = false;
        bool listIsEqualZero = false;

        int totalQtyInList = 0;
        for (int i = 0; i < size; i++) {
          if (_listAddNew[i]) {
            if(textListExpiryDate[i].text.isEmpty) {
              listEmptyDate = true;
              break;
            } else {
              if(textListQty[i].text.isEmpty) {
                listIsInPending = true;
                break;
              } else {
                if(textListQty[i].text == "0") {
                  listIsEqualZero = true;
                  break;
                } else {
                  totalQtyInList = totalQtyInList + int.parse(textListQty[i].text);
                }
              }
            }
          }
        }

        if (listEmptyDate) {
          showToast("Please add Expiry Date before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        } else {
          if (listIsInPending) {
            showToast("Please add Quantity before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          } else {
            if (listIsEqualZero) {
              showToast("Please enter Quantity more than 0", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            } else {
              SharedPreferences prefs = await SharedPreferences.getInstance();
              int totalInboundQty = int.parse(prefs.getString('inboundQty').toString());

              int currentTotalQty = 0;
              for (int i = 0; i < _listItemId.length ; i++) {
                if (_listAddNew[i]) {
                  int listQty = int.parse(textListQty[i].text);
                  currentTotalQty = currentTotalQty + listQty;
                }
              }

              if (totalInboundQty == currentTotalQty) {
                addListingToList();
              } else {
                showToast("Total Quantity not equal to Inbound Quantity", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
            }
          }
        }
      } else {
        showToast("Please add Expiry Date before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
    else{
      addListingToList();
    }
  }

  addListingToList() {
    for (int i = 0; i < _listItemId.length ; i++) {
      _listExpiryDate[i] = textListExpiryDate[i].text;
      _listQuantity[i] = textListQty[i].text;
    }

    callCreateApi();
  }

  Future<void> callCreateApi() async{
    int size = _listItemId.length;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String batchId = prefs.getString('batchId').toString();

    List<BulkData> _listBulkData = [];

    for(int i = 0 ; i < size ; i++){
      String listItemId = _listItemId[i];
      if(listItemId.isEmpty) {
        String listExpDate = _listExpiryDate[i];
        String listQty = _listQuantity[i];
        _listBulkData.add(BulkData(listExpDate, listQty));
      }
    }

    var details = new Map();
    details['bulkData'] = _listBulkData;
    details['batchId'] = batchId;

    final response = await cubit.createSkuLocationInBulk(details);
    String responseInString = await response;

    var res = json.decode(responseInString);
    CreateInbSkuLocationBulk _res = CreateInbSkuLocationBulk.fromJson(res);

    if (_res.ret == "0") {
      CreateInbSkuLocationBulkData _resData = CreateInbSkuLocationBulkData.fromJson(_res.data.toJson());

      int responseSize = _resData.skuLocation.length;

      for (int i = 0; i < responseSize; i++) {
        String responseItemId = _resData.skuLocation[i].id.toString();

        _listItemId[i] = responseItemId;
        _listAddNew[i] = false;
      }
    }

    doneUpdate = 1;

    isAddNewButton = false;
    isNotNewButton = true;

    setState(() {

    });
  }

  Future<void> checkBatchLocationData() async{

    int size = _listItemId.length;

    if (size > 0) {
      for (int i = 0; i < size; i++) {
        final response = await cubit.getBatchLocationWithBatchIdAndSkuId(_listItemId[i]);
        String responseInString = await response;

        if (responseInString == "") {

        }
        else {
          var res = json.decode(responseInString);
          GetInbBatchLocation _res = GetInbBatchLocation.fromJson(res);
          GetInbBatchLocationData _resData = GetInbBatchLocationData.fromJson(
              _res.data.toJson());

          if (_res.ret == "0") {
            if (_resData.totalCount > 0) {
              _listCompleteBatch[i] = true;
            }
          }
        }
      }
    }

    setState(() {

    });
  }
}
