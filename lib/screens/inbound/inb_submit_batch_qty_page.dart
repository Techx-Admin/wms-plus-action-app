import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/bloc/inbound/batch_container_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/batch_expiry_date_header_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/batch_serial_no_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/submit_batch_qty_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/create_inb_request_dispute_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/create_inb_sku_batch_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/create_inb_sku_location_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/get_inb_sku_batch_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/get_inb_sku_info_model.dart';
import 'package:wms_plus_action/model/inbound/submitbatchqty/get_inb_sku_location_model.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:progress_dialog/progress_dialog.dart';

import 'inb_batch_container_page.dart';
import 'inb_batch_expiry_date_header_page.dart';
import 'inb_batch_serial_no_page.dart';

class InbSubmitBatchQtyPage extends StatefulWidget {
  String skuId, skuCode, barcode, totalQty;

  InbSubmitBatchQtyPage(
      {Key? key,
        required this.skuId,
        required this.skuCode,
        required this.barcode,
        required this.totalQty})
      : super(key: key);

  @override
  _InbSubmitBatchQtyPageState createState() => _InbSubmitBatchQtyPageState();
}

class _InbSubmitBatchQtyPageState extends State<InbSubmitBatchQtyPage> {
  late SubmitBatchQtyCubit cubit;
  int isSerial = 0, isExpired = 0;
  bool isVisibleBtnSubmit = false;
  bool isVisibleBtnNext = false;
  bool isVisibleInboundQtyEnable = false;
  bool isVisibleInboundQtyDisable = false;
  TextEditingController textActualInboundQty = TextEditingController();
  String barcode = "";
  late ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('skuId', widget.skuId);
    prefs.setString('skuCode', widget.skuCode);
    prefs.setString('barcode', widget.barcode);
    prefs.setString('totalQty', widget.totalQty);

    final responseOne = await cubit.getSkuInfo(widget.skuId);
    String responseOneInString = await responseOne;

    if (responseOneInString == "") {

    }
    else {
      var res = json.decode(responseOneInString);
      GetInbSkuInfo _res = GetInbSkuInfo.fromJson(res);

      if (_res.ret == "0") {
        GetInbSkuInfoData _resData = GetInbSkuInfoData.fromJson(_res.data.toJson());

        for (int i = 0; i < _resData.records.length; i++) {
          isExpired = _resData.records[i].expiry;
          isSerial = _resData.records[i].serialCode;

          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('skuIsExpired', isExpired.toString());
          prefs.setString('skuIsSerial', isSerial.toString());
        }

        bool existSkuForThisBatch = false;
        final responseTwo = await cubit.getSkuBatchInfo();
        String responseTwoInString = await responseTwo;

        if(responseTwoInString != null) {
          var res = json.decode(responseTwoInString);
          GetInbSkuBatch _res = GetInbSkuBatch.fromJson(res);

          if (_res.ret == "0") {
            int skuSize = 0;

            GetInbSkuBatchData _resData = GetInbSkuBatchData.fromJson(_res.data.toJson());

            skuSize = _resData.records.length;

            for (int i = 0; i < skuSize; i++) {
              String batchId = _resData.records[i].id.toString();
              String skuIdInList = _resData.records[i].skuId.toString();
              String inboundQty = _resData.records[i].qty.toString();

              if (skuIdInList == widget.skuId) {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setString('batchId', batchId);
                prefs.setString('inboundQty', inboundQty);

                textActualInboundQty.text = inboundQty;

                isVisibleInboundQtyEnable = false;
                isVisibleInboundQtyDisable = true;

                isVisibleBtnNext = true;

                existSkuForThisBatch = true;

                break;
              }
            }
          }
        }

        if(! existSkuForThisBatch) {
          isVisibleBtnSubmit = true;

          isVisibleInboundQtyEnable = true;
          isVisibleInboundQtyDisable = false;
        }

        setState(() {

        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<SubmitBatchQtyCubit>();
    progressDialog = ProgressDialog(context);

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 110,

                        child: Column(
                          children: [
                            Visibility(
                              visible: isVisibleBtnSubmit,
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      checkTextFieldEmptyOrNot();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty.all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Visibility(
                              visible: isVisibleBtnNext,
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      proceedNextPage();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty.all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Card(
                      margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Wrap(
                        children: <Widget>[

                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kAssignTrayTopBgColor,
                              ),
                              child: Column(
                                children: [

                                  // show barcode
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 10.0),
                                    child: Text(
                                      widget.barcode,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kInbListAdapterTextColor,
                                          fontSize: 17,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),

                                  // show qty
                                  Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(left: 10.0, right: 10.0, top: 5.0, bottom: 15.0, ),

                                    child: RichText(
                                      textAlign: TextAlign.center,
                                      text: TextSpan(children: <TextSpan>[
                                        TextSpan(
                                            text: "Quantity : ",
                                            style: TextStyle(
                                                color: ColorConstants.kInbBatchQtyTextColor,
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400
                                            )),
                                        TextSpan(
                                            text: widget.totalQty,
                                            style: TextStyle(
                                                color: ColorConstants.kInbRedColor,
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400
                                            )),
                                      ]),
                                    ),
                                  ),

                                ],
                              )),

                          // Actual Qty Header
                          Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 30.0),
                            child: Text(
                              'Actual Inbound Quantity',
                              style: TextStyle(
                                  color: ColorConstants.kInbDriverLabelTextColor,
                                  fontSize: 14,
                                  fontFamily: 'Montserrat',
                                  fontWeight: FontWeight.w400),
                            ),
                          ),

                          // Actual Qty Textfield Enable
                          Visibility(
                            visible: isVisibleInboundQtyEnable,
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 30.0),
                                child: TextField(
                                  controller: textActualInboundQty,
                                  enabled: true,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.sentences,
                                )
                            ),
                          ),
                          // Actual Qty Textfield Disable
                          Visibility(
                            visible: isVisibleInboundQtyDisable,
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(left: 20.0, right: 20.0, top: 5.0, bottom: 30.0),
                                child: TextField(
                                  controller: textActualInboundQty,
                                  enabled: false,
                                  style: TextStyle(
                                      color: ColorConstants.kPrimaryColor,
                                      fontSize: 16,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(),
                                  ),
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.sentences,
                                )
                            ),
                          ),

                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> proceedNextPage () async{
    // 1 - Batch Serial No Page
    // 2 - Batch Container Page
    // 3 - Batch Expiry Date Page
    // 4 - call api
    int intentPage = 0;
    int intentSerialNoPage = 1;
    int intentContainerPage = 2;
    int intentExpiryDatePage = 3;
    int intentCallApiPage = 4;

    if(isSerial == 1 && isExpired == 1){
      intentPage = intentSerialNoPage;
    }
    else if(isSerial == 1 && isExpired == 0){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String skuLocationId = prefs.getString('skuLocationId').toString();

      if (skuLocationId == '') {
        await progressDialog.show();
        intentPage = intentCallApiPage;
      }
      else {
        intentPage = intentSerialNoPage;
      }
    }
    else if(isSerial == 0 && isExpired == 1) {
      intentPage = intentExpiryDatePage;
    }
    else {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String skuLocationId = prefs.getString('skuLocationId').toString();

      if (skuLocationId == '') {
        await progressDialog.show();
        intentPage = intentCallApiPage;
      }
      else {
        intentPage = intentContainerPage;
      }
    }

    if(intentPage == intentSerialNoPage) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) =>
                    BatchSerialNoCubit(InboundRepository()),
                child: InbBatchSerialNoPage(),
              )));
    }
    else if(intentPage == intentContainerPage) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) =>
                    BatchContainerCubit(InboundRepository()),
                child: InbBatchContainerPage(),
              )));
    }
    else if(intentPage == intentExpiryDatePage) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                create: (context) =>
                    BatchExpiryDateHeaderCubit(InboundRepository()),
                child: InbBatchExpiryDateHeaderPage(),
              )));
    }
    else if(intentPage == intentCallApiPage) {

      final response = await cubit.getSkuLocationId();
      String responseInString = await response;

      if (responseInString == "") {
        await progressDialog.hide();

        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        var res = json.decode(responseInString);
        GetInbSkuLocation _res = GetInbSkuLocation.fromJson(res);

        if (_res.ret == "0") {
          int totalRecord = _res.data.totalCount;
          if (totalRecord > 0) {
            int skuSize = _res.data.records.length;

            for (int i = 0; i < skuSize; i++) {
              String skuLocationId = _res.data.records[i].id.toString();

              SharedPreferences prefs = await SharedPreferences.getInstance();
              prefs.setString('skuLocationId', skuLocationId);
            }

            await progressDialog.hide();

            if (isSerial == 1 && isExpired == 0) {

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) =>
                            BatchSerialNoCubit(InboundRepository()),
                        child: InbBatchSerialNoPage(),
                      )));

            } else if (isSerial == 0 && isExpired == 0) {

              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BlocProvider(
                        create: (context) =>
                            BatchContainerCubit(InboundRepository()),
                        child: InbBatchContainerPage(),
                      )));

            }
          } else {
            await progressDialog.hide();

            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        } else {
          await progressDialog.hide();

          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
      }
    }
  }

  checkTextFieldEmptyOrNot() {
    String newInboundQty = textActualInboundQty.text;

    if (newInboundQty == '') {
      showToast("Please Enter Inbound Quantity", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      _checkInternetConnection(newInboundQty);
    }
  }

  Future<void> _checkInternetConnection(String newInboundQty) async {
    await progressDialog.show();

    bool showToastFlag = false;

    try {
      final response = await InternetAddress.lookup('www.google.com');
      if (response.isNotEmpty) {

        final responseOne = await cubit.createSkuBatch(newInboundQty);
        String responseOneInString = await responseOne;

        if(responseOneInString != null) {

          var res = json.decode(responseOneInString);
          CreateInbSkuBatch _res = CreateInbSkuBatch.fromJson(res);

          if (_res.ret == "0") {
            String batchId = _res.data.skuBatch.id.toString();

            int oriQty = int.parse(widget.totalQty);
            int inboundQty = int.parse(textActualInboundQty.text);
            int disputeQty = oriQty - inboundQty;

            SharedPreferences prefs = await SharedPreferences.getInstance();
            prefs.setString('batchId', batchId);
            prefs.setString('inboundQty', inboundQty.toString());

            isVisibleInboundQtyEnable = false;
            isVisibleInboundQtyDisable = true;

            isVisibleBtnSubmit = false;
            isVisibleBtnNext = true;

            if (disputeQty == 0) {
              if (isSerial == 1 && isExpired == 1) {
                await progressDialog.hide();

                showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                proceedNextPage();
              }
              else if (isSerial == 1 && isExpired == 0) {
                final responseTwo = await cubit.createSkuLocationInSingle(inboundQty.toString());
                String responseTwoInString = await responseTwo;

                if(responseTwoInString != null) {

                  var resTwo = json.decode(responseTwoInString);
                  CreateInbSkuLocation _resTwo = CreateInbSkuLocation.fromJson(resTwo);

                  if (_resTwo.ret == "0") {
                    String skuLocationId = _resTwo.data.skuLocation.id.toString();

                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString('skuLocationId', skuLocationId);

                    await progressDialog.hide();

                    showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                    proceedNextPage();
                  }
                  else {
                    showToastFlag = true;
                  }
                }
                else {
                  showToastFlag = true;
                }

              }
              else if (isSerial == 0 && isExpired == 1) {
                await progressDialog.hide();

                showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                proceedNextPage();
              }
              else {
                final responseTwo = await cubit.createSkuLocationInSingle(inboundQty.toString());
                String responseTwoInString = await responseTwo;

                if(responseTwoInString != null) {

                  var resTwo = json.decode(responseTwoInString);
                  CreateInbSkuLocation _resTwo = CreateInbSkuLocation.fromJson(resTwo);

                  if (_resTwo.ret == "0") {
                    String skuLocationId = _resTwo.data.skuLocation.id.toString();

                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString('skuLocationId', skuLocationId);

                    await progressDialog.hide();

                    showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                    proceedNextPage();
                  }
                  else {
                    showToastFlag = true;
                  }
                }
                else {
                  showToastFlag = true;
                }


              }
            }
            else {
              final responseTwo = await cubit.createInboundDispute(oriQty, disputeQty);
              String responseTwoInString = await responseTwo;

              if(responseTwoInString != null) {

                var resTwo = json.decode(responseTwoInString);
                CreateInbRequestDispute _resTwo = CreateInbRequestDispute.fromJson(resTwo);

                if (_resTwo.ret == "0") {
                  String inboundQty = textActualInboundQty.text;

                  if (isSerial == 1 && isExpired == 1) {

                    await progressDialog.hide();

                    showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                    proceedNextPage();
                  }
                  else if (isSerial == 1 && isExpired == 0) {
                    final responseTwo = await cubit.createSkuLocationInSingle(inboundQty.toString());
                    String responseTwoInString = await responseTwo;

                    if(responseTwoInString != null) {

                      var resTwo = json.decode(responseTwoInString);
                      CreateInbSkuLocation _resTwo = CreateInbSkuLocation.fromJson(resTwo);

                      if (_resTwo.ret == "0") {
                        String skuLocationId = _resTwo.data.skuLocation.id.toString();

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString('skuLocationId', skuLocationId);

                        await progressDialog.hide();

                        showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                        proceedNextPage();
                      }
                      else {
                        showToastFlag = true;
                      }
                    }
                    else {
                      showToastFlag = true;
                    }

                  }
                  else if (isSerial == 0 && isExpired == 1) {
                    await progressDialog.hide();

                    showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                    proceedNextPage();
                  }
                  else {
                    final responseTwo = await cubit.createSkuLocationInSingle(inboundQty.toString());
                    String responseTwoInString = await responseTwo;

                    if(responseTwoInString != null) {

                      var resTwo = json.decode(responseTwoInString);
                      CreateInbSkuLocation _resTwo = CreateInbSkuLocation.fromJson(resTwo);

                      if (_resTwo.ret == "0") {
                        String skuLocationId = _resTwo.data.skuLocation.id.toString();

                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        prefs.setString('skuLocationId', skuLocationId);

                        await progressDialog.hide();

                        showToast("Inbound Quantity Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                        proceedNextPage();
                      }
                      else {
                        showToastFlag = true;
                      }
                    }
                    else {
                      showToastFlag = true;
                    }
                  }

                }
                else {
                  showToastFlag = true;
                }
              }
              else {
                showToastFlag = true;
              }
            }
          }
          else {
            showToastFlag = true;
          }
        }
        else {
          showToastFlag = true;
        }
      }
      else {
        await progressDialog.hide();
      }

      if(showToastFlag) {
        await progressDialog.hide();

        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }

    } on SocketException catch (err) {
      await progressDialog.hide();

      showToast(StringConstants.util_alert_no_connection_msg, Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
