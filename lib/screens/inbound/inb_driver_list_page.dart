import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wms_plus_action/bloc/inbound/driver_edit_cubit.dart';
import 'package:wms_plus_action/bloc/inbound/driver_list_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/model/inbound/driverlist/get_inb_driver_list_model.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:need_resume/need_resume.dart';

import 'inb_driver_edit_page.dart';

class InbDriverListPage extends StatefulWidget {
  String inboundId;

  InbDriverListPage({Key? key, required this.inboundId}) : super(key: key);

  @override
  _InbDriverListPageState createState() => _InbDriverListPageState();
}

class _InbDriverListPageState extends ResumableState<InbDriverListPage> {
  late DriverListCubit cubit;
  List<String> _listRequestDeliveryId = [];
  List<String> _listRequestId = [];
  List<String> _listDriverName = [];
  List<String> _listDriverIcNo = [];
  List<String> _listDriverContact = [];
  List<String> _listCarPlateNo = [];
  List<String> _listCarParkAt = [];
  List<String> _listStatus = [];

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getInboundDriverList(widget.inboundId);
    });
  }

  @override
  void onResume() {
    switch (resume.source) {
      case 'driver_list_screen':
        refreshPage(resume.data.toString());
        break;
    }
  }

  Future<void> refreshPage(data) async{
    if(data == "true") {
      // nothing happen
    }
    else {
      _listRequestDeliveryId.clear();
      _listRequestId.clear();
      _listDriverName.clear();
      _listDriverIcNo.clear();
      _listDriverContact.clear();
      _listCarPlateNo.clear();
      _listCarParkAt.clear();
      _listStatus.clear();

      final response = await cubit.getInboundDriverListWithoutEmit(data);
      String responseInString = await response;

      var res = json.decode(responseInString);
      GetInbDriverList _res = GetInbDriverList.fromJson(res);
      GetInbDriverListData _resData = GetInbDriverListData.fromJson(
          _res.data.toJson());

      for (int i = 0; i < _resData.inboundRequestDelivery.length; i++) {
        String requestDeliveryId = _resData.inboundRequestDelivery[i].id
            .toString();
        String requestId = _resData.inboundRequestDelivery[i].requestId
            .toString();
        String driverName = _resData.inboundRequestDelivery[i].driverName;
        String driverIcNo = _resData.inboundRequestDelivery[i].driverIcNo;
        String driverContact = _resData.inboundRequestDelivery[i].driverContact;
        String carPlateNo = _resData.inboundRequestDelivery[i].carPlateNo;
        String carParkAt = _resData.inboundRequestDelivery[i].carParkAt;
        String status = _resData.inboundRequestDelivery[i].status.toString();

        _listRequestDeliveryId.add(requestDeliveryId);
        _listRequestId.add(requestId);
        _listDriverName.add(driverName);
        _listDriverIcNo.add(driverIcNo);
        _listDriverContact.add(driverContact);
        _listCarPlateNo.add(carPlateNo);
        _listCarParkAt.add(carParkAt);
        _listStatus.add(status);
      }

      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<DriverListCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            var res = json.decode(responseInString);
            GetInbDriverList _res = GetInbDriverList.fromJson(res);
            GetInbDriverListData _resData = GetInbDriverListData.fromJson(_res.data.toJson());

            for (int i = 0; i < _resData.inboundRequestDelivery.length; i++) {
              String requestDeliveryId =
                  _resData.inboundRequestDelivery[i].id.toString();
              String requestId =
                  _resData.inboundRequestDelivery[i].requestId.toString();
              String driverName = _resData.inboundRequestDelivery[i].driverName;
              String driverIcNo = _resData.inboundRequestDelivery[i].driverIcNo;
              String driverContact =
                  _resData.inboundRequestDelivery[i].driverContact;
              String carPlateNo = _resData.inboundRequestDelivery[i].carPlateNo;
              String carParkAt = _resData.inboundRequestDelivery[i].carParkAt;
              String status =
                  _resData.inboundRequestDelivery[i].status.toString();

              _listRequestDeliveryId.add(requestDeliveryId);
              _listRequestId.add(requestId);
              _listDriverName.add(driverName);
              _listDriverIcNo.add(driverIcNo);
              _listDriverContact.add(driverContact);
              _listCarPlateNo.add(carPlateNo);
              _listCarParkAt.add(carParkAt);
              _listStatus.add(status);
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 120,

                        child: Column(
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width,
                                margin: const EdgeInsets.only(
                                    left: 60.0, right: 60.0, bottom: 10.0),
                                child: new ElevatedButton(
                                  child: Text(
                                    'Add',
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kGenPicklistButtonTextColor,
                                        fontSize: 15,
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w600),
                                  ),
                                  onPressed: () {
                                    push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => BlocProvider(
                                                  create: (context) =>
                                                      DriverEditCubit(
                                                          InboundRepository()),
                                                  child: InbDriverEditPage(
                                                      requestDeliveryId: "",
                                                      requestId: widget.inboundId,
                                                      driverName: "",
                                                      driverIcNo: "",
                                                      driverContact: "",
                                                      carPlateNo: "",
                                                      carParkAt: "",
                                                      status: "",
                                                      typeOfData: "new"),
                                                )),
                                        'driver_list_screen');
                                  },
                                  style: ButtonStyle(
                                    shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                        RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(25.0),
                                    )),
                                    backgroundColor: MaterialStateProperty.all(
                                        ColorConstants.kThemeColor),
                                    padding: MaterialStateProperty.all(
                                        EdgeInsets.only(top: 10, bottom: 10)),
                                  ),
                                )),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: const EdgeInsets.only(
                        bottom: 160.0, top: 20.0, left: 20.0, right: 20.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kAssignTrayTopBgColor,
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  'Driver',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color:
                                          ColorConstants.kAssignTrayHeaderColor,
                                      fontSize: 17,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )),

                          // listview
                          Expanded(
                            child: Container(
                              child: ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: _listRequestDeliveryId.length,
                                  separatorBuilder: (BuildContext context,
                                          int index) =>
                                      Divider(
                                          height: 1,
                                          color:
                                              ColorConstants.kInbListLineColor),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: [
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 3,
                                              child: GestureDetector(
                                                onTap: () {

                                                  push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => BlocProvider(
                                                            create: (context) =>
                                                                DriverEditCubit(
                                                                    InboundRepository()),
                                                            child: InbDriverEditPage(
                                                                requestDeliveryId: _listRequestDeliveryId[index],
                                                                requestId: _listRequestId[index],
                                                                driverName: _listDriverName[index],
                                                                driverIcNo: _listDriverIcNo[index],
                                                                driverContact: _listDriverContact[index],
                                                                carPlateNo: _listCarPlateNo[index],
                                                                carParkAt: _listCarParkAt[index],
                                                                status: _listStatus[index],
                                                                typeOfData: "edit"),
                                                          )),
                                                      'driver_list_screen');

                                                },
                                                child: Container(
                                                    padding: const EdgeInsets.only(left: 20.0,top: 10.0, bottom: 10.0),
                                                    width: MediaQuery.of(context)
                                                        .size
                                                        .width,
                                                    child: Text(
                                                      _listDriverName[index],
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kInbListAdapterTextColor,
                                                          fontSize: 12,
                                                          fontFamily:
                                                              'Montserrat',
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    )),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 2,
                                              child: GestureDetector(
                                                onTap: () {

                                                  push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => BlocProvider(
                                                            create: (context) =>
                                                                DriverEditCubit(
                                                                    InboundRepository()),
                                                            child: InbDriverEditPage(
                                                                requestDeliveryId: _listRequestDeliveryId[index],
                                                                requestId: _listRequestId[index],
                                                                driverName: _listDriverName[index],
                                                                driverIcNo: _listDriverIcNo[index],
                                                                driverContact: _listDriverContact[index],
                                                                carPlateNo: _listCarPlateNo[index],
                                                                carParkAt: _listCarParkAt[index],
                                                                status: _listStatus[index],
                                                                typeOfData: "edit"),
                                                          )),
                                                      'driver_list_screen');

                                                },
                                                child: Container(
                                                    padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                                                    width: MediaQuery.of(context)
                                                        .size
                                                        .width,
                                                    child: Text(
                                                      _listCarPlateNo[index],
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kInbListAdapterTextColor,
                                                          fontSize: 12,
                                                          fontFamily:
                                                              'Montserrat',
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
