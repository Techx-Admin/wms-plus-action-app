import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:wms_plus_action/bloc/inbound/batch_container_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wms_plus_action/model/inbound/batchcontainer/body_inb_batch_location_bulk_model.dart';
import 'package:wms_plus_action/model/inbound/batchcontainer/create_inb_batch_location_bulk_model.dart';
import 'package:wms_plus_action/model/inbound/batchcontainer/get_inb_batch_location_model.dart';
import 'package:wms_plus_action/model/inbound/batchcontainer/get_inb_container_info_model.dart';
import 'package:wms_plus_action/screens/general/gen_action_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InbBatchContainerPage extends StatefulWidget {
  @override
  _InbBatchContainerPageState createState() => _InbBatchContainerPageState();
}

class _InbBatchContainerPageState extends State<InbBatchContainerPage> {
  late BatchContainerCubit cubit;
  int doneUpdate = 0;
  bool isVisibleNotSubmit = false;
  bool isVisibleBtnSubmit = false;
  bool isVisibleContentEmpty = false;
  List<String> _listItemId = [];
  List<String> _listContainerId = [];
  List<String> _listContainerCode = [];
  List<String> _listContainerStatus = [];
  List<String> _listQty = [];
  List<String> _listBarcode = [];
  List<String> _listTotalQty = [];
  List<TextEditingController> textListQty = [];
  String sharedBarcode = "", sharedTotalQty = "";

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sharedBarcode = prefs.getString('barcode').toString();
    sharedTotalQty = prefs.getString('totalQty').toString();

    cubit.getBatchLocationWithBatchId();
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<BatchContainerCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataBatchContainerState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            bool emptyFlag = false;

            _listItemId.clear();
            _listContainerId.clear();
            _listContainerCode.clear();
            _listContainerStatus.clear();
            _listQty.clear();
            _listBarcode.clear();
            _listTotalQty.clear();

            if (responseInString == "") {
              emptyFlag = true;
            }
            else {
              var res = json.decode(responseInString);
              GetInbBatchLocation _res = GetInbBatchLocation.fromJson(res);
              GetInbBatchLocationData _resData = GetInbBatchLocationData.fromJson(_res.data.toJson());

              if (_res.ret == "0") {
                if (_resData.totalCount > 0) {
                  int itemSize = _resData.records.length;

                  for (int i = 0; i < itemSize; i++) {
                    String itemId = _resData.records[i].id.toString();
                    String containerId = _resData.records[i].containerRunningCodeId.toString();
                    String containerCode = _resData.records[i].containerRunningCode;
                    String batchQty = _resData.records[i].batchQty.toString();

                    _listItemId.add(itemId);
                    _listContainerId.add(containerId);
                    _listContainerCode.add(containerCode);
                    _listContainerStatus.add("0");
                    _listQty.add(batchQty);
                    _listBarcode.add(sharedBarcode);
                    _listTotalQty.add(sharedTotalQty);
                  }

                  doneUpdate = 1;
                } else {
                  emptyFlag = true;
                }
              } else {
                emptyFlag = true;
              }
            }

            if (doneUpdate == 1) {
              isVisibleBtnSubmit = false;
              isVisibleNotSubmit = true;
            }
            else {
              if (emptyFlag) {
                isVisibleContentEmpty = true;
              }

              isVisibleBtnSubmit = true;
              isVisibleNotSubmit = false;
            }

            setState(() {

            });
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 120,

                        child: Column(
                          children: [
                            Visibility(
                              visible: isVisibleBtnSubmit,
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      submitData();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty
                                          .all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Ink(
                                      height: 60,
                                      padding: const EdgeInsets.all(5.0),
                                      decoration: ShapeDecoration(
                                        color: ColorConstants.kThemeColor,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Image.asset(
                                          'images/icon_topbar_scan.png',
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          scanQr();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  //listview data
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(bottom: 100.0),
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listItemId.length,
                          itemBuilder: (BuildContext context, int index) {
                            textListQty.add(new TextEditingController());

                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  child: Card(
                                    margin: const EdgeInsets.only(top: 20.0,
                                        left: 20.0,
                                        right: 20.0,
                                        bottom: 10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    elevation: 10,
                                    child: Wrap(
                                      children: <Widget>[

                                        // listview header
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(
                                                      10.0),
                                                  topRight: Radius.circular(
                                                      10.0)),
                                              color: ColorConstants
                                                  .kAssignTrayTopBgColor,
                                            ),
                                            child: Column(
                                              children: [

                                                // show barcode
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 10.0,
                                                      right: 10.0,
                                                      top: 10.0),
                                                  child: Text(
                                                    sharedBarcode,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 17,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight: FontWeight
                                                            .w400),
                                                  ),
                                                ),

                                                // show qty
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                    left: 10.0,
                                                    right: 10.0,
                                                    top: 5.0,
                                                    bottom: 15.0,),

                                                  child: RichText(
                                                    textAlign: TextAlign.center,
                                                    text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                              text: "Quantity : ",
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: sharedTotalQty,
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbRedColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                        ]),
                                                  ),
                                                ),
                                              ],
                                            )),

                                        // container code
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 30.0),
                                          child: Text(
                                            'Container Code',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0),
                                            child: TextField(
                                              controller: TextEditingController(
                                                  text: _listContainerCode[index]),
                                              enabled: false,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(),
                                              ),
                                              textInputAction: TextInputAction.next,
                                              textCapitalization: TextCapitalization.sentences,
                                            )
                                        ),

                                        // qty
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 20.0),
                                          child: Text(
                                            'Enter Inbound Quantity',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Visibility(
                                          visible: isVisibleBtnSubmit,
                                          child: Container(
                                              width: MediaQuery
                                                  .of(context)
                                                  .size
                                                  .width,
                                              margin: const EdgeInsets.only(
                                                  left: 20.0,
                                                  right: 20.0,
                                                  top: 5.0,
                                                  bottom: 30.0),
                                              child: TextField(
                                                controller: textListQty[index],
                                                enabled: true,
                                                style: TextStyle(
                                                    color: ColorConstants.kPrimaryColor,
                                                    fontSize: 16,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w400),
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(),
                                                ),
                                                textInputAction: TextInputAction.done,
                                                keyboardType: TextInputType.number,
                                                inputFormatters: <TextInputFormatter>[
                                                  FilteringTextInputFormatter.digitsOnly
                                                ],
                                              )
                                          ),
                                        ),
                                        Visibility(
                                          visible: isVisibleNotSubmit,
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0,
                                                bottom: 30.0),
                                            child: Text(
                                              _listQty[index],
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                )
                              ],
                            );
                          }),
                    ),
                  ),

                  // top with empty
                  Visibility(
                    visible: isVisibleContentEmpty,
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      margin: const EdgeInsets.only(bottom: 150.0),
                      child: Card(
                        margin: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          children: <Widget>[

                            // page header
                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                child: Column(
                                  children: [

                                    // show barcode
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 10.0, top: 10.0),
                                      child: Text(
                                        sharedBarcode,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 17,
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),

                                    // show qty
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(left: 10.0,
                                        right: 10.0,
                                        top: 5.0,
                                        bottom: 15.0,),

                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: "Quantity : ",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbBatchQtyTextColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                          TextSpan(
                                              text: sharedTotalQty,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbRedColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                        ]),
                                      ),
                                    ),

                                  ],
                                )),

                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 200,
                                child: Center(
                                  child: Text(
                                    'No Container Code yet ?',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kInbListAdapterTextColor,
                                        fontSize: 12,
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),

                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scanQr() async {
    String barcode = "-1";
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      barcode = await FlutterBarcodeScanner.scanBarcode(
          '#ff6666', 'Cancel', true, ScanMode.QR);
    } on Exception {

    }
    if (barcode == "-1") {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {

      if(doneUpdate == 0) {
        final response = await cubit.getContainerInfoApi(barcode);
        String responseInString = await response;

        var res = json.decode(responseInString);
        GetInbContainerInfo _res = GetInbContainerInfo.fromJson(res);

        bool validContainer = false;

        if (responseInString == "") {
          showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        } else {
          if (_res.ret == "0") {

            GetInbContainerInfoData _resData = GetInbContainerInfoData.fromJson(_res.data.toJson());

            if (_resData.totalCount == 0) {
              showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
            else {
              int inboundSize = _resData.records.length;

              for (int i = 0; i < inboundSize; i++) {
                String listContainerId = _resData.records[i].id.toString();
                String listContainerCode = _resData.records[i].containerRunningCode;
                int listStatus = _resData.records[i].status;

                if (listContainerCode == barcode && listStatus == 0) {
                  _listItemId.add("");
                  _listContainerId.add(listContainerId);
                  _listContainerCode.add(listContainerCode);
                  _listContainerStatus.add("1");
                  _listQty.add("");
                  _listBarcode.add(sharedBarcode);
                  _listTotalQty.add(sharedTotalQty);

                  validContainer = true;

                  break;
                }
              }

              if (validContainer) {
                isVisibleContentEmpty = false;

                setState(() {

                });
              }
              else {
                showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
            }
          } else {
            showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      }
      else{
        showToast("Currently cannot add Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future<void> submitData() async {

    int size = _listItemId.length;

    if(size == 0) {
      showToast("Please add Container Code before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {
      // check qty inside list
      // if qty = pending, ask user to key in
      // if qty = 0, ask user to key in more than 0
      bool listIsEqualZero = false;
      bool listIsInPending = false;
      for(int i = 0 ; i < size ; i++){
        if(textListQty[i].text.isEmpty) {
          listIsInPending = true;
          break;
        }
        else {
          if(textListQty[i].text == "0") {
            listIsEqualZero = true;
            break;
          }
        }
      }

      if(listIsInPending) {
        showToast("Please add Quantity before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        if (listIsEqualZero) {
          showToast("Please enter Quantity more than 0", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
        else {
          // check total qty inside list that must equal inbound qty
          SharedPreferences prefs = await SharedPreferences.getInstance();
          int totalInboundQty = int.parse(prefs.getString('inboundQty').toString());
          int currentTotalQty = 0;
          for (int i = 0; i < _listItemId.length; i++) {
            int listQty = int.parse(textListQty[i].text);
            currentTotalQty = currentTotalQty + listQty;
          }

          if (totalInboundQty == currentTotalQty) {

            SharedPreferences prefs = await SharedPreferences.getInstance();
            String batchId = prefs.getString('batchId').toString();
            String skuLocationId = prefs.getString('skuLocationId').toString();

            List<BulkData> _listBulkData = [];

            for(int i = 0 ; i < size ; i++){
              String listItemId = _listItemId[i];
              String listContainerId = _listContainerId[i];
              String listQty = textListQty[i].text ;

              if(listItemId.isEmpty) {
                _listBulkData.add(BulkData(listContainerId, skuLocationId, listQty));
              }
            }

            var details = new Map();
            details['bulkData'] = _listBulkData;
            details['batchId'] = batchId;

            final responseOne = await cubit.createBatchLocationInBulk(details);
            String responseOneInString = await responseOne;

            if (responseOneInString == "") {
              showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
            else {
              var resOne = json.decode(responseOneInString);

              CreateInbBatchLocationBulk _resOne = CreateInbBatchLocationBulk.fromJson(resOne);

              if (_resOne.ret == "0") {
                int responseSize = _resOne.data.batchLocation.length;
                for (int j = 0; j < responseSize; j++) {
                  String responseItemId = _resOne.data.batchLocation[j].id.toString();
                  String responseContainerRunningId = _resOne.data.batchLocation[j].containerRunningCodeId;

                  int listSize = _listItemId.length;
                  for (int k = 0; k < listSize; k++) {
                    String listContainerId = _listContainerId[k];
                    String listContainerStatus = _listContainerStatus[k];

                    if (listContainerId == responseContainerRunningId && listContainerStatus == "1") {
                      _listItemId[k] = responseItemId;
                    }
                  }
                }

                final responseTwo = await cubit.updateInboundRequestItemStatus();
                String responseTwoInString = await responseTwo;

                showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) =>
                        GenActionPage(),
                  ),
                      (route) => false,
                );
              }
              else {
                showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
            }
          }
          else {
            showToast("Total Quantity not equal to Inbound Quantity", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      }
    }
  }
}
