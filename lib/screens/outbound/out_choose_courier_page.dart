import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:wms_plus_action/bloc/outbound/choose_courier_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/bloc/outbound/scan_order_cubit.dart';
import 'package:wms_plus_action/model/outbound/choosecourier/get_list_logistic_and_order_model.dart';
import 'package:wms_plus_action/repo/out_repo.dart';
import 'package:wms_plus_action/screens/outbound/out_scan_order_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:need_resume/need_resume.dart';

class OutChooseCourierPage extends StatefulWidget {

  @override
  _OutChooseCourierPageState createState() => _OutChooseCourierPageState();
}

class _OutChooseCourierPageState extends ResumableState<OutChooseCourierPage> {
  late ChooseCourierCubit cubit;
  List<String> _listLogisticName = [];
  List<String> _listOrderCount = [];

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getListOfLogisticAndTotalOrder();
    });
  }

  @override
  void onResume() {
    switch (resume.source) {
      case 'choose_courier_screen':
        checkNeedRefresh(resume.data.toString());
        break;
    }
  }

  Future<void> checkNeedRefresh(data) async{
    if(data == "1") {
      _listLogisticName.clear();
      _listOrderCount.clear();

      final response = await cubit.getListOfLogisticAndTotalOrderWithoutEmit();
      String responseInString = await response;

      var res = json.decode(responseInString);
      GetListLogisticAndOrder _res = GetListLogisticAndOrder.fromJson(res);
      GetListLogisticAndOrderData _resData = GetListLogisticAndOrderData.fromJson(_res.data.toJson());

      for (int i = 0; i < _resData.logisticPartnerList.length; i++) {
        String logisticName = _resData.logisticPartnerList[i].logisticPartber;
        String orderCount = _resData.logisticPartnerList[i].orderCount.toString();

        _listLogisticName.add(logisticName);
        _listOrderCount.add(orderCount);
      }

      setState(() { });
    }
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<ChooseCourierCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadedChooseCourierState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            var res = json.decode(responseInString);
            GetListLogisticAndOrder _res = GetListLogisticAndOrder.fromJson(res);
            GetListLogisticAndOrderData _resData = GetListLogisticAndOrderData.fromJson(_res.data.toJson());

            for (int i = 0; i < _resData.logisticPartnerList.length; i++) {
              String logisticName = _resData.logisticPartnerList[i].logisticPartber;
              String orderCount = _resData.logisticPartnerList[i].orderCount.toString();

              _listLogisticName.add(logisticName);
              _listOrderCount.add(orderCount);
            }

          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,

                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_back.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop(true);
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_menu.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {

                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: const EdgeInsets.only(bottom: 100.0, top: 20.0, left: 20.0, right: 20.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kAssignTrayTopBgColor,
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  'Outbound',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color:
                                      ColorConstants.kAssignTrayHeaderColor,
                                      fontSize: 17,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )),

                          Expanded(
                            child: Container(
                              child: ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: _listLogisticName.length,
                                  separatorBuilder:
                                      (BuildContext context, int index) =>
                                      Divider(
                                          height: 1,
                                          color: ColorConstants.kAssignTrayLineColor),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: [
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 45,
                                              child: GestureDetector(
                                                onTap: () {
                                                  String orderForCurrent = _listOrderCount[index];

                                                  if(orderForCurrent == "0"){

                                                  }
                                                  else {
                                                    push(
                                                        context,
                                                        MaterialPageRoute(
                                                            builder: (
                                                                context) =>
                                                                BlocProvider(
                                                                  create: (
                                                                      context) =>
                                                                      ScanOrderCubit(
                                                                          OutboundRepository()),
                                                                  child: OutScanOrderPage(
                                                                      logisticName: _listLogisticName[index]),
                                                                )),
                                                        'choose_courier_screen'
                                                    );
                                                  }
                                                },
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    padding: new EdgeInsets.only(left: 20.0),
                                                    margin: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                                                    child: Text(
                                                      _listLogisticName[index],
                                                      style: TextStyle(
                                                          color: ColorConstants.kInbListAdapterTextColor,
                                                          fontSize: 12,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w400),
                                                    )
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 15,
                                              child: GestureDetector(
                                                onTap: () {
                                                  push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) => BlocProvider(
                                                            create: (context) => ScanOrderCubit(OutboundRepository()),
                                                            child: OutScanOrderPage(
                                                                logisticName: _listLogisticName[index] ),
                                                          )), 'choose_courier_screen'
                                                  );
                                                },
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    padding: new EdgeInsets.only(right: 20.0),
                                                    margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                                                    child: Text(
                                                      _listOrderCount[index],
                                                      textAlign: TextAlign.center,
                                                      style: TextStyle(
                                                          color: ColorConstants.kInbListAdapterTextColor,
                                                          fontSize: 12,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w400),
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}