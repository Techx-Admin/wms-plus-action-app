import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/src/provider.dart';
import 'package:wms_plus_action/bloc/pick/assign_tray_cubit.dart';
import 'package:wms_plus_action/bloc/pick/generate_list_cubit.dart';
import 'package:wms_plus_action/model/pick/generatelist/check_pick_detail_model.dart';
import 'package:wms_plus_action/model/pick/generatelist/get_pick_info_model.dart';
import 'package:wms_plus_action/model/pick/generatelist/grab_picklist_model.dart';
import 'package:wms_plus_action/repo/pic_repo.dart';
import 'package:wms_plus_action/screens/general/gen_action_page.dart';
import 'package:wms_plus_action/screens/pick/pic_assign_tray_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wms_plus_action/utils/string_utils.dart';

class PicGenerateListPage extends StatefulWidget {
  PicGenerateListPage({Key? key}) : super(key: key);

  @override
  _PicGenerateListPageState createState() => _PicGenerateListPageState();
}

class _PicGenerateListPageState extends State<PicGenerateListPage> {
  late GenerateListCubit cubit;
  TextEditingController textCompleted = TextEditingController();
  TextEditingController textRemaining = TextEditingController();
  bool isVisible = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getPickInfoUser();
    });
  }

  Future<void> assignOrderPick(
      BuildContext context, GenerateListCubit cubit) async {
    String responseInString = "";

    final response = await cubit.assignOrderPick();
    responseInString = await response;

    var res = json.decode(responseInString);
    try {
      GrabPicklist _resModel = GrabPicklist.fromJson(res);
      GrabPicklistData dataData =
          GrabPicklistData.fromJson(_resModel.data.toJson());
      String pickId = dataData.orderPickId.toString();

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                    create: (context) => AssignTrayCubit(PickRepository()),
                    child: PicAssignTrayPage(pickId: pickId),
                  )));
    } catch (e) {
      print(e);
      showToast(StringConstants.pick_toast_not_have_available_picklist,
          Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
  }

  Future<void> checkOrderPickDetail(
      BuildContext context, GenerateListCubit cubit) async {
    String responseInString = "";

    final response = await cubit.checkOrderPickDetail();
    setState(() {
      responseInString = response;
    });

    var res = json.decode(responseInString);
    try {
      CheckPickDetail _resModel = CheckPickDetail.fromJson(res);
      CheckPickDetailData dataData =
          CheckPickDetailData.fromJson(_resModel.data.toJson());
      String pickId = dataData.orderPickDetails.id.toString();

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                    create: (context) => AssignTrayCubit(PickRepository()),
                    child: PicAssignTrayPage(pickId: pickId),
                  )));
    } catch (e) {
      print(e);

      setState(() {
        isVisible = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<GenerateListCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => GenActionPage(),
          ),
          (route) => false,
        );
        return true;
      },
      child: BlocListener(
          listener: (context, state) {
            if (state is LoadDataGenerateListState && state.words != null) {
              String responseInString = "";

              setState(() {
                responseInString = state.words;
              });

              var res = json.decode(responseInString);
              GetPickInfo _resModel = GetPickInfo.fromJson(res);
              GetPickInfoData dataData =
                  GetPickInfoData.fromJson(_resModel.data.toJson());

              textCompleted.text = dataData.userCompleted.toString();
              textRemaining.text = dataData.availableOrderPick.toString();

              checkOrderPickDetail(context, cubit);
            }
          },
          bloc: cubit,
          child: Scaffold(
            backgroundColor: ColorConstants.kWhiteColor,
            body: SafeArea(
                top: true,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: <Widget>[
                      // center
                      Align(
                          alignment: Alignment.center,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 30.0, right: 30.0),
                                    child: Text(
                                      'Completed Picklist',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 20.0, top: 10.0),
                                    child: TextField(
                                      controller: textCompleted,
                                      enabled: false,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextFieldColor,
                                          fontSize: 20,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder()),
                                      textAlign: TextAlign.center,
                                    )
                                ),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 30.0, right: 30.0, top: 50.0),
                                    child: Text(
                                      'Remaining Picklist',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 20.0, top: 10.0),
                                    child: TextField(
                                      controller: textRemaining,
                                      enabled: false,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextFieldColor,
                                          fontSize: 20,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                      ),
                                      textAlign: TextAlign.center,
                                    )),
                                Visibility(
                                  visible: isVisible,
                                  maintainSize: true,
                                  maintainAnimation: true,
                                  maintainState: true,
                                  child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      margin: const EdgeInsets.only(
                                          left: 60.0, right: 60.0, top: 50.0),
                                      child: new ElevatedButton(
                                        child: Text(
                                          'Grab Picklist',
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistButtonTextColor,
                                              fontSize: 15,
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                        onPressed: () {
                                          assignOrderPick(context, cubit);
                                        },
                                        style: ButtonStyle(
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25.0),
                                          )),
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  ColorConstants.kThemeColor),
                                          padding: MaterialStateProperty.all(
                                              EdgeInsets.only(
                                                  top: 10, bottom: 10)),
                                        ),
                                      )),
                                ),
                              ])),

                      // bottom
                      Positioned(
                          bottom: 20.0,
                          right: 0.0,
                          left: 0.0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60,

                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                GenActionPage(),
                                          ),
                                          (route) => false,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                )
                              ],
                            ),
                          )),
                    ],
                  ),
                )),
          )
      ),
    );
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
