import 'package:flutter/material.dart';
import 'package:wms_plus_action/screens/general/gen_action_page.dart';
import 'package:wms_plus_action/utils/color_utils.dart';

class PicCompletePage extends StatefulWidget {
  @override
  _PicCompletePageState createState() => _PicCompletePageState();
}

class _PicCompletePageState extends State<PicCompletePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          top: true,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                // center
                Align(
                    alignment: Alignment.center,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            height: 80,
                            alignment: Alignment.center, // This is needed
                            child: Image.asset(
                              "images/icon_action_done.png",
                              fit: BoxFit.contain,
                              width: 70,
                              color: ColorConstants.kThemeColor,
                            ),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(top: 20.0),
                              child: Text(
                                'Picking Process Completed',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: ColorConstants.kGenPicklistTextColor,
                                    fontSize: 20,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              )),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(top: 15.0),
                              child: Text(
                                'Please Pass All The Tray To The Next Station',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: ColorConstants.kGenPicklistTextColor,
                                    fontSize: 12,
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              )),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(
                                  left: 60.0, right: 60.0, top: 20.0),
                              child: new ElevatedButton(
                                child: Text(
                                  'Done',
                                  style: TextStyle(
                                      color: ColorConstants
                                          .kGenPicklistButtonTextColor,
                                      fontSize: 15,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w600),
                                ),
                                onPressed: () {
                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          GenActionPage(),
                                    ),
                                    (route) => false,
                                  );
                                },
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorConstants.kThemeColor),
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.only(top: 10, bottom: 10)),
                                ),
                              )),
                        ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
