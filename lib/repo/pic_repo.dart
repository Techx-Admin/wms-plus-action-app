import 'dart:io';
import 'dart:convert';

import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wms_plus_action/service/http_service.dart';

class PickRepository {
  Future<String> getPickInfoUser() async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getPickInfoUrl;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {

    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> checkOrderPickDetail() async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.checkOrderPickDetailsUrl;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {

    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> assignOrderPick() async{
    try {
      Map data = {

      };
      var body = json.encode(data);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.assignOrderPickUrl, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getOrderPickDetails(String pickId) async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getOrderPickDetailsUrl;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'pickId': pickId,
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getTrayInfo(String trayCode) async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getTrayInfo;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'trayCode': trayCode,
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> assignTrayToOrderPick(String orderPickTrayId, String trayId) async{
    try {
      Map data = {
        'orderPickTrayId': '$orderPickTrayId',
        'trayId': '$trayId'
      };
      var body = json.encode(data);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.assignTrayToOrderPick, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateTrayStatus(String trayId) async{
    try {
      Map data = {
        'trayId': '$trayId',
        'status': '1'
      };
      var body = json.encode(data);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateTrayStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getListPickItem(String pickId) async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getListPickItem;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'pickId': '$pickId'
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderPickItemLocationInBulk(Map details) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      var body = json.encode(details);

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateOrderPickItemLocationInBulk, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> updateOrderPick(String pickId) async{
    try {
      Map data = {
        'pickId': '$pickId',
        'status': '1'
      };
      var body = json.encode(data);

      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateOrderPick, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderItemStatus(String orderItemId, String pickDate) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'orderItemId': orderItemId,
        'status': '2',
        'pickBy': '1',
        'checkBy': '0',
        'packBy': '0',
        'pickDate': pickDate
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateOrderItemStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderStatus(String orderId) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'orderId': orderId,
        'status': '3'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateOrderStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }
}