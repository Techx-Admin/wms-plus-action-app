import 'dart:core';
import 'dart:io';
import 'dart:convert';

import 'package:wms_plus_action/service/http_service.dart';
import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InboundRepository {

  Future<String> getInboundRequestList() async{
    var fullUrl = StringConstants.InboundBaseUrl + StringConstants.getInboundRequestList;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '50',
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getInboundRequestDetail(String requestId) async{
    var fullUrl = StringConstants.InboundBaseUrl + StringConstants.getInboundRequestDetail;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '50',
      'requestId': requestId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getInboundDriverList(String requestId) async{
    var fullUrl = StringConstants.InboundBaseUrl + StringConstants.getInboundDriverList;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'requestId': requestId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createInboundDelivery(String requestId, String driverName,
      String driverIcNo, String carPlateNo, String carParkAt) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'requestId': requestId,
        'driverName': driverName,
        'driverIcNo': driverIcNo,
        'carPlateNo': carPlateNo,
        'carParkAt': carParkAt,
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.createInboundDelivery, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateInboundDelivery(String requestId, String requestDeliveryId,
      String driverName, String driverIcNo, String carPlateNo, String carParkAt) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'requestId': requestId,
        'requestDeliveryId': requestDeliveryId,
        'driverName': driverName,
        'driverIcNo': driverIcNo,
        'carPlateNo': carPlateNo,
        'carParkAt': carParkAt,
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.updateInboundDelivery, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getSkuInfo(String skuId) async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getSkuInfo;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'skuId' : skuId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getSkuBatchInfo() async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getSkuBatchInfo;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String reqItemId = prefs.getString('requestItemId').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '30',
      'reqItemId' : reqItemId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  // submit batch qty page
  // batch expiry date header page
  Future<String> getSkuLocationId() async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getSkuLocationId;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String batchId = prefs.getString('batchId').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'batchId' : batchId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createSkuBatch(String newQty) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();
      String requestId = prefs.getString('requestId').toString();
      String requestItemId = prefs.getString('requestItemId').toString();

      Map data = {
        'requestId': requestId,
        'reqItemId': requestItemId,
        'qty': newQty
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.createSkuBatch, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createSkuLocationInSingle(String newQty) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();
      String batchId = prefs.getString('batchId').toString();

      Map data = {
        'batchId': batchId,
        'mfgDate': "",
        'expDate': "",
        'qty': newQty
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.createSkuLocationInSingle, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createInboundDispute(int oriQty, int disputeQty) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();
      String requestId = prefs.getString('requestId').toString();
      String requestItemId = prefs.getString('requestItemId').toString();
      String batchId = prefs.getString('batchId').toString();

      Map data = {
        'requestId': requestId,
        'requestItemId':requestItemId,
        'batchId': batchId,
        'oriAmt': oriQty,
        'disputeAmt': disputeQty
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.createInboundDispute, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getSerialNoInBulk() async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getSerialNoInBulk;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String batchId = prefs.getString('batchId').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '30',
      'batchId' : batchId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createSerialNoInBulk(Map details) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      var body = json.encode(details);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.createSerialNoInBulk, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> getBatchLocationWithBatchId() async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getBatchLocationWithBatchId;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String batchId = prefs.getString('batchId').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'batchId' : batchId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getContainerInfoApi(String containerRunningCode) async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getContainerInfoApi;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '20',
      'containerRunningCode' : containerRunningCode
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createBatchLocationInBulk(Map details) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      var body = json.encode(details);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.createBatchLocationInBulk, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> updateInboundRequestItemStatus() async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();
      String requestItemId = prefs.getString('requestItemId').toString();
      String requestId = prefs.getString('requestId').toString();

      Map data = {
        'itemId': requestItemId,
        'requestId':requestId,
        'status': '1'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.updateInboundRequestItemStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getInboundRequestDispute() async{
    var fullUrl = StringConstants.InboundBaseUrl + StringConstants.getInboundRequestDispute;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String requestId = prefs.getString('requestId').toString();

    Map<String, String> qParams = {
      'requestId': requestId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateInboundDisputeInBulk(Map details) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      var body = json.encode(details);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.updateInboundDisputeInBulk, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> updateInboundRequestStatus() async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();
      String requestId = prefs.getString('requestId').toString();

      Map data = {
        'requestId': requestId,
        'status': '1',
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          2, StringConstants.updateInboundRequestStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> createSkuLocationInBulk(Map details) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      var body = json.encode(details);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.createSkuLocationInBulk, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      print('error Future caught: $e');
      return '';
    }
  }

  Future<String> getBatchLocationWithBatchIdAndSkuId(String skuId) async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getBatchLocationWithBatchId;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();
    String batchId = prefs.getString('batchId').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
      'batchId' : batchId,
      'skuLocationId' : skuId
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }
}