import 'dart:io';
import 'dart:convert';

import 'package:wms_plus_action/utils/string_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wms_plus_action/service/http_service.dart';

class OutboundRepository {
  Future<String> getListOfLogisticAndTotalOrder() async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getListOfLogisticAndTotalOrder;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {

    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> getListOfOrderCode(String logisticPartner) async{
    var fullUrl = StringConstants.orderBaseUrl + StringConstants.getListOfOrderCode;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'logisticPartner': logisticPartner
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderStatusToSix(String orderId) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'orderId': orderId,
        'status': '6'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          5, StringConstants.updateOrderStatusToSix, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }
}