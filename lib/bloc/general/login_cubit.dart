import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/gen_repo.dart';

class LoginCubit extends Cubit<LoginState>{
  final GeneralRepository _repository;

  LoginCubit(this._repository) : super(SuccessState());

  Future<String> checkLogin(String userName, String password) async {
    try {
      String responseFromRepo = await _repository.callLogin(userName, password);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class LoginState {}

class SuccessState extends LoginState { }

class FailState extends LoginState { }