import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RequestDetailCubit extends Cubit<RequestDetailState>{
  final InboundRepository _repository;

  RequestDetailCubit(this._repository) : super(BeforeReqDetailState());

  Future getInboundRequestDetail(String requestId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('requestId', requestId);

    emit(LoadingReqDetailState());
  
    try {
      String responseFromRepo = await _repository.getInboundRequestDetail(requestId);

      if(responseFromRepo == '' ){
        emit(FailReqDetailState());
      } else {
        emit(LoadDataReqDetailState(responseFromRepo));
        emit(ReadyReqDetailState());
      }
    } on Exception catch (e) {
      emit(FailReqDetailState());
    }
  }
}

abstract class RequestDetailState {}

class BeforeReqDetailState extends RequestDetailState { }

class LoadingReqDetailState extends RequestDetailState { }

class LoadDataReqDetailState extends RequestDetailState {
  final String words;

  LoadDataReqDetailState(this.words);
}

class ReadyReqDetailState extends RequestDetailState { }

class FailReqDetailState extends RequestDetailState { }