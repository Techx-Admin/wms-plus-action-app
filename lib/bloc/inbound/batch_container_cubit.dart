import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';

class BatchContainerCubit extends Cubit<BatchContainerState>{
  final InboundRepository _repository;

  BatchContainerCubit(this._repository) : super(BeforeBatchContainerState());

  Future getBatchLocationWithBatchId() async {
    emit(LoadingBatchContainerState());

    try {
      String responseFromRepo = await _repository.getBatchLocationWithBatchId();

      if(responseFromRepo == '' ){
        emit(FailBatchContainerState());
      } else {
        emit(LoadDataBatchContainerState(responseFromRepo));
        emit(ReadyBatchContainerState());
      }
    } on Exception catch (e) {
      emit(FailBatchContainerState());
    }
  }

  Future getContainerInfoApi(String containerRunningCode) async {
    try {
      String responseFromRepo = await _repository.getContainerInfoApi(containerRunningCode);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createBatchLocationInBulk(Map details) async {
    try {
      String responseFromRepo = await _repository.createBatchLocationInBulk(details);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future updateInboundRequestItemStatus() async {
    try {
      String responseFromRepo = await _repository.updateInboundRequestItemStatus();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class BatchContainerState {}

class BeforeBatchContainerState extends BatchContainerState { }

class LoadingBatchContainerState extends BatchContainerState { }

class LoadDataBatchContainerState extends BatchContainerState {
  final String words;

  LoadDataBatchContainerState(this.words);
}

class ReadyBatchContainerState extends BatchContainerState { }

class FailBatchContainerState extends BatchContainerState { }