import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';

class SubmitDisputeCubit extends Cubit<SubmitDisputeState>{
  final InboundRepository _repository;

  SubmitDisputeCubit(this._repository) : super(BeforeSubmitDisputeState());

  Future getInboundRequestDispute() async {
    emit(LoadingSubmitDisputeState());

    try {
      String responseFromRepo = await _repository.getInboundRequestDispute();

      if(responseFromRepo == '' ){
        emit(FailSubmitDisputeState());
      } else {
        emit(LoadDataSubmitDisputeState(responseFromRepo));
        emit(ReadySubmitDisputeState());
      }
    } on Exception catch (e) {
      emit(FailSubmitDisputeState());
    }
  }

  Future updateInboundDisputeInBulk(Map details) async {
    try {
      String responseFromRepo = await _repository.updateInboundDisputeInBulk(details);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future updateInboundRequestStatus() async {
    try {
      String responseFromRepo = await _repository.updateInboundRequestStatus();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class SubmitDisputeState {}

class BeforeSubmitDisputeState extends SubmitDisputeState { }

class LoadingSubmitDisputeState extends SubmitDisputeState { }

class LoadDataSubmitDisputeState extends SubmitDisputeState {
  final String words;

  LoadDataSubmitDisputeState(this.words);
}

class ReadySubmitDisputeState extends SubmitDisputeState { }

class FailSubmitDisputeState extends SubmitDisputeState { }