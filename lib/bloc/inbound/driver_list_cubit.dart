import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';

class DriverListCubit extends Cubit<DriverListState>{
  final InboundRepository _repository;

  DriverListCubit(this._repository) : super(BeforeState());

  Future getInboundDriverList(String requestId) async {
    emit(LoadingState());

    try {
      String responseFromRepo = await _repository.getInboundDriverList(requestId);

      if(responseFromRepo == '' ){
        emit(FailState());
      } else {
        emit(LoadDataState(responseFromRepo));
        emit(ReadyState());
      }
    } on Exception catch (e) {
      emit(FailState());
    }
  }

  Future getInboundDriverListWithoutEmit(String requestId) async {
    try {
      String responseFromRepo = await _repository.getInboundDriverList(requestId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class DriverListState {}

class BeforeState extends DriverListState { }

class LoadingState extends DriverListState { }

class LoadDataState extends DriverListState {
  final String words;

  LoadDataState(this.words);
}

class ReadyState extends DriverListState { }

class FailState extends DriverListState { }