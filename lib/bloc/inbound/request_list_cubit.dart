import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/inb_repo.dart';

class RequestListCubit extends Cubit<RequestListState>{
  final InboundRepository _repository;

  RequestListCubit(this._repository) : super(BeforeRequestListState());

  Future getInboundRequestList() async {
    emit(LoadingRequestListState());

    try {
      String responseFromRepo = await _repository.getInboundRequestList();

      if(responseFromRepo == '' ){
        emit(FailRequestListState());
      } else {
        emit(LoadDataRequestListState(responseFromRepo));
        emit(ReadyRequestListState());
      }
    } on Exception catch (e) {
      emit(FailRequestListState());
    }
  }
}

abstract class RequestListState {}

class BeforeRequestListState extends RequestListState { }

class LoadingRequestListState extends RequestListState { }

class LoadDataRequestListState extends RequestListState {
  final String words;

  LoadDataRequestListState(this.words);
}

class ReadyRequestListState extends RequestListState { }

class FailRequestListState extends RequestListState { }