import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/pic_repo.dart';

class GenerateListCubit extends Cubit<GenerateListState>{
  final PickRepository _repository;

  GenerateListCubit(this._repository) : super(BeforeGenerateListState());

  Future getPickInfoUser() async {
    emit(LoadingGenerateListState());

    try {
      String responseFromRepo = await _repository.getPickInfoUser();

      if(responseFromRepo == '' ){
        emit(FailGenerateListState());
      } else {
        emit(LoadDataGenerateListState(responseFromRepo));
        emit(ReadyGenerateListState());
      }
    } on Exception catch (e) {
      emit(FailGenerateListState());
    }
  }

  Future<String> checkOrderPickDetail() async {
    try {
      String responseFromRepo = await _repository.checkOrderPickDetail();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> assignOrderPick() async {
    try {
      String responseFromRepo = await _repository.assignOrderPick();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class GenerateListState {}

class BeforeGenerateListState extends GenerateListState { }

class LoadingGenerateListState extends GenerateListState { }

class LoadDataGenerateListState extends GenerateListState {
  final String words;

  LoadDataGenerateListState(this.words);
}

class ReadyGenerateListState extends GenerateListState { }

class FailGenerateListState extends GenerateListState { }