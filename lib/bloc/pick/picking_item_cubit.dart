import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_plus_action/repo/pic_repo.dart';

class PickingItemCubit extends Cubit<PickingItemState>{
  final PickRepository _repository;

  PickingItemCubit(this._repository) : super(BeforePickingItemState());

  Future getOrderPickDetails(String pickId) async {
    emit(LoadingPickingItemState());

    try {
      String responseFromRepo = await _repository.getListPickItem(pickId);

      if(responseFromRepo == '' ){
        emit(FailPickingItemState());
      } else {
        emit(LoadedPickingItemState(responseFromRepo));
        emit(ReadyPickingItemState());
      }
    } on Exception catch (e) {
      emit(FailPickingItemState());
    }
  }

  Future<String> updateOrderPickItemLocationInBulk(Map details) async {
    try {
      String responseFromRepo = await _repository.updateOrderPickItemLocationInBulk(details);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderPick(String pickId) async {
    try {
      String responseFromRepo = await _repository.updateOrderPick(pickId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderItemStatus(String orderItemId, String pickDate) async {
    try {
      String responseFromRepo = await _repository.updateOrderItemStatus(orderItemId, pickDate);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateOrderStatus(String orderId) async {
    try {
      String responseFromRepo = await _repository.updateOrderStatus(orderId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class PickingItemState {}

class BeforePickingItemState extends PickingItemState { }

class LoadingPickingItemState extends PickingItemState { }

class LoadedPickingItemState extends PickingItemState {
  final String words;

  LoadedPickingItemState(this.words);
}

class ReadyPickingItemState extends PickingItemState { }

class FailPickingItemState extends PickingItemState { }