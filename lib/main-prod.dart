import 'package:flutter/material.dart';
import 'package:wms_plus_action/screens/general/gen_splash_page.dart';
import 'flavors.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async{
  F.appFlavor = Flavor.PROD;

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: F.title,

        // https://www.geeksforgeeks.org/safearea-in-flutter/
        home: SafeArea(
          top: true,
          child: GenSplashPage(),
        )
    );
  }
}