enum Flavor {
  DEV,
  PROD,
}

class F {
  static Flavor? appFlavor;

  static String get name => appFlavor?.name ?? '';

  static String get title {
    switch (appFlavor) {
      case Flavor.DEV:
        return 'Dev-Plus';
      case Flavor.PROD:
        return 'Plus Action';
      default:
        return 'title';
    }
  }

}
