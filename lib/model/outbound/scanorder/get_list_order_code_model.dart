class GetListOrderCode {
  late String ret;
  late GetListOrderCodeData data;

  GetListOrderCode({required this.ret, required this.data});

  GetListOrderCode.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetListOrderCodeData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetListOrderCodeData {
  late OrderLogisticPartnerData orderLogisticPartnerData;

  GetListOrderCodeData({required this.orderLogisticPartnerData});

  GetListOrderCodeData.fromJson(Map<String, dynamic> json) {
    orderLogisticPartnerData = (json['orderLogisticPartnerData'] != null
        ? OrderLogisticPartnerData.fromJson(
        json['orderLogisticPartnerData'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderLogisticPartnerData != null) {
      data['orderLogisticPartnerData'] = this.orderLogisticPartnerData.toJson();
    }
    return data;
  }
}

class OrderLogisticPartnerData {
  late String name;
  late int orderCount;
  late List<OrderData> orderData;

  OrderLogisticPartnerData({required this.name, required this.orderCount, required this.orderData});

  OrderLogisticPartnerData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    orderCount = json['order_count'];
    if (json['orderData'] != null) {
      orderData = <OrderData>[];
      json['orderData'].forEach((v) {
        orderData.add(new OrderData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['order_count'] = this.orderCount;
    if (this.orderData != null) {
      data['orderData'] = this.orderData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderData {
  late int id;
  late String orderCode;
  late int companyId;
  late int clientId;
  late String cusOrderNo;
  late int orderType;
  // Null trackingId;
  late String trackingId;
  late String trackingCode;
  // Null logisticPartnerId;
  late String logisticPartnerId;
  late String logisticPartner;
  late int status;
  late String cdate;
  late String cby;

  OrderData(
      {required this.id,
        required this.orderCode,
        required this.companyId,
        required this.clientId,
        required this.cusOrderNo,
        required this.orderType,
        required this.trackingId,
        required this.trackingCode,
        required this.logisticPartnerId,
        required this.logisticPartner,
        required this.status,
        required this.cdate,
        required this.cby});

  OrderData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderCode = json['order_code'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    cusOrderNo = json['cus_order_no'];
    orderType = json['order_type'];
    trackingId = json['tracking_id'];
    trackingCode = json['tracking_code'];
    logisticPartnerId = json['logistic_partner_id'];
    logisticPartner = json['logistic_partner'];
    status = json['status'];
    cdate = json['cdate'];
    cby = json['cby'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_code'] = this.orderCode;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['cus_order_no'] = this.cusOrderNo;
    data['order_type'] = this.orderType;
    data['tracking_id'] = this.trackingId;
    data['tracking_code'] = this.trackingCode;
    data['logistic_partner_id'] = this.logisticPartnerId;
    data['logistic_partner'] = this.logisticPartner;
    data['status'] = this.status;
    data['cdate'] = this.cdate;
    data['cby'] = this.cby;
    return data;
  }
}