class GetListLogisticAndOrder {
  late String ret;
  late GetListLogisticAndOrderData data;

  GetListLogisticAndOrder({required this.ret, required this.data});

  GetListLogisticAndOrder.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetListLogisticAndOrderData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetListLogisticAndOrderData {
  late List<LogisticPartnerList> logisticPartnerList;

  GetListLogisticAndOrderData({required this.logisticPartnerList});

  GetListLogisticAndOrderData.fromJson(Map<String, dynamic> json) {
    if (json['logisticPartnerList'] != null) {
      logisticPartnerList = <LogisticPartnerList>[];
      json['logisticPartnerList'].forEach((v) {
        logisticPartnerList.add(new LogisticPartnerList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.logisticPartnerList != null) {
      data['logisticPartnerList'] =
          this.logisticPartnerList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogisticPartnerList {
  late String logisticPartber;
  late int orderCount;

  LogisticPartnerList({required this.logisticPartber, required this.orderCount});

  LogisticPartnerList.fromJson(Map<String, dynamic> json) {
    logisticPartber = json['logisticPartber'];
    orderCount = json['orderCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['logisticPartber'] = this.logisticPartber;
    data['orderCount'] = this.orderCount;
    return data;
  }
}