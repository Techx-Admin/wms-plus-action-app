class BulkData {
  late String containerRunningCodeId;
  late String skuLocationId;
  late String batchQty;

  BulkData(
      this.containerRunningCodeId,
      this.skuLocationId,
      this.batchQty);

  Map toJson() => {
    'containerRunningCodeId': containerRunningCodeId,
    'skuLocationId': skuLocationId,
    'batchQty': batchQty,
  };
}

class Bulk {
  late String batchId;
  late List<BulkData> bulkData;

  Bulk(this.batchId, this.bulkData);

  Map toJson() => {
    'batchId': batchId,
    'bulkData': bulkData
  };
}