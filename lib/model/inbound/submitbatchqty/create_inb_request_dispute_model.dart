class CreateInbRequestDispute {
  late String ret;

  CreateInbRequestDispute({required this.ret});

  CreateInbRequestDispute.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    return data;
  }
}