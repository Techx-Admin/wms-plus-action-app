class UpdateInbRequestStatus {
  late String ret;
  late UpdateInbRequestStatusData data;

  UpdateInbRequestStatus({required this.ret, required this.data});

  UpdateInbRequestStatus.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? UpdateInbRequestStatusData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UpdateInbRequestStatusData {
  late int updatedRow;

  UpdateInbRequestStatusData({required this.updatedRow});

  UpdateInbRequestStatusData.fromJson(Map<String, dynamic> json) {
    updatedRow = json['updatedRow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updatedRow'] = this.updatedRow;
    return data;
  }
}