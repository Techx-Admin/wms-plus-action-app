class UpdateInbDisputeBulk {
  late String ret;

  UpdateInbDisputeBulk({required this.ret});

  UpdateInbDisputeBulk.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    return data;
  }
}