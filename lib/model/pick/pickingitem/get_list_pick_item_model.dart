class GetListPickItem {
  late String ret;
  late GetListPickItemData data;

  GetListPickItem({required this.ret, required this.data});

  factory GetListPickItem.fromJson(Map<String, dynamic> json) => GetListPickItem(
    ret: json["ret"],
    data: GetListPickItemData.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "ret": ret,
    "data": data.toJson(),
  };
}

class GetListPickItemData {
  late List<List<OrderPickItemLocation>> orderPickItemLocation;

  GetListPickItemData({required this.orderPickItemLocation});

  factory GetListPickItemData.fromJson(Map<String, dynamic> json) =>
      GetListPickItemData(orderPickItemLocation: List<List<OrderPickItemLocation>>.from(json["OrderPickItemLocation"].map((x) =>
      List<OrderPickItemLocation>.from(x.map((x) => OrderPickItemLocation.fromJson(x))))),
  );

  Map<String, dynamic> toJson() => {
    "OrderPickItemLocation": List<dynamic>.from(orderPickItemLocation.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
  };
}

class OrderPickItemLocation {
  OrderPickItemLocation({
    required this.id,
    required this.pickId,
    required this.orderItemId,
    required this.skuLocationId,
    required this.trayId,
    required this.qty,
    required this.status,
    required this.skuCode,
    required this.skuName,
    required this.locationLabel,
    required this.trayCode,
  });

  int id;
  int pickId;
  int orderItemId;
  int skuLocationId;
  int trayId;
  int qty;
  int status;
  String skuCode;
  String skuName;
  String locationLabel;
  String trayCode;

  factory OrderPickItemLocation.fromJson(Map<String, dynamic> json) => OrderPickItemLocation(
    id: json["id"],
    pickId: json["pick_id"],
    orderItemId: json["order_item_id"],
    skuLocationId: json["sku_location_id"],
    trayId: json["tray_id"],
    qty: json["qty"],
    status: json["status"],
    skuCode: json["sku_code"],
    skuName: json["sku_name"],
    locationLabel: json["location_label"],
    trayCode: json["tray_code"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pick_id": pickId,
    "order_item_id": orderItemId,
    "sku_location_id": skuLocationId,
    "tray_id": trayId,
    "qty": qty,
    "status": status,
    "sku_code": skuCode,
    "sku_name": skuName,
    "location_label": locationLabel,
    "tray_code": trayCode,
  };
}