class CheckPickDetail {
  late String ret;
  late CheckPickDetailData data;

  CheckPickDetail({required this.ret, required this.data});

  CheckPickDetail.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new CheckPickDetailData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CheckPickDetailData {
  late OrderPickDetails orderPickDetails;

  CheckPickDetailData({required this.orderPickDetails});

  CheckPickDetailData.fromJson(Map<String, dynamic> json) {
    orderPickDetails = (json['orderPickDetails'] != null
        ? new OrderPickDetails.fromJson(json['orderPickDetails'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderPickDetails != null) {
      data['orderPickDetails'] = this.orderPickDetails.toJson();
    }
    return data;
  }
}

class OrderPickDetails {
  late int id;
  late int companyId;
  late String pickCode;
  late String assignDate;
  late String assignTo;
  late String completedAt;
  late int status;

  OrderPickDetails(
      {required this.id,
        required this.companyId,
        required this.pickCode,
        required this.assignDate,
        required this.assignTo,
        required this.completedAt,
        required this.status });

  OrderPickDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    pickCode = json['pick_code'];
    assignDate = json['assign_date'];
    assignTo = json['assign_to'];
    completedAt = json['completed_at'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['pick_code'] = this.pickCode;
    data['assign_date'] = this.assignDate;
    data['assign_to'] = this.assignTo;
    data['completed_at'] = this.completedAt;
    data['status'] = this.status;
    return data;
  }
}