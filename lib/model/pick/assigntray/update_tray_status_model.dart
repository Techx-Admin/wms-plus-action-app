class UpdateTrayStatus {
  late String ret;
  late UpdateTrayStatusData data;

  UpdateTrayStatus({required this.ret, required this.data});

  UpdateTrayStatus.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new UpdateTrayStatusData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UpdateTrayStatusData {
  late int updatedRow;

  UpdateTrayStatusData({required this.updatedRow});

  UpdateTrayStatusData.fromJson(Map<String, dynamic> json) {
    updatedRow = json['updatedRow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updatedRow'] = this.updatedRow;
    return data;
  }
}