class GetOrderPickDetail {
  late String ret;
  late GetOrderPickDetailData data;

  GetOrderPickDetail({required this.ret, required this.data});

  GetOrderPickDetail.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null
        ? GetOrderPickDetailData.fromJson(json['data'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetOrderPickDetailData {
  late OrderPickDetails orderPickDetails;

  GetOrderPickDetailData({required this.orderPickDetails});

  GetOrderPickDetailData.fromJson(Map<String, dynamic> json) {
    orderPickDetails = (json['orderPickDetails'] != null
        ? new OrderPickDetails.fromJson(json['orderPickDetails'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.orderPickDetails != null) {
      data['orderPickDetails'] = this.orderPickDetails.toJson();
    }
    return data;
  }
}

class OrderPickDetails {
  late int id;
  late int companyId;
  late String pickCode;
  late String assignDate;
  late String assignTo;
  // Null completedAt;
  late String completedAt;
  late int status;
  late List<OrderPickTray> orderPickTray;

  OrderPickDetails(
      {required this.id,
      required this.companyId,
      required this.pickCode,
      required this.assignDate,
      required this.assignTo,
      required this.completedAt,
      required this.status,
      required this.orderPickTray});

  OrderPickDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    pickCode = json['pick_code'];
    assignDate = json['assign_date'];
    assignTo = json['assign_to'];
    completedAt = json['completed_at'];
    status = json['status'];
    if (json['order_pick_tray'] != null) {
      orderPickTray = <OrderPickTray>[];
      json['order_pick_tray'].forEach((v) {
        orderPickTray.add(new OrderPickTray.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['pick_code'] = this.pickCode;
    data['assign_date'] = this.assignDate;
    data['assign_to'] = this.assignTo;
    data['completed_at'] = this.completedAt;
    data['status'] = this.status;
    if (this.orderPickTray != null) {
      data['order_pick_tray'] =
          this.orderPickTray.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderPickTray {
  late int id;
  late int orderId;
  late int pickId;
  late int trayId;
  late int status;
  late Order order;
  late String trayCode;

  OrderPickTray(
      {required this.id,
      required this.orderId,
      required this.pickId,
      required this.trayId,
      required this.status,
      required this.order,
      required this.trayCode});

  OrderPickTray.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    pickId = json['pick_id'];
    trayId = json['tray_id'];
    status = json['status'];
    order = (json['order'] != null ? new Order.fromJson(json['order']) : null)!;
    trayCode = json['tray_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.orderId;
    data['pick_id'] = this.pickId;
    data['tray_id'] = this.trayId;
    data['status'] = this.status;
    if (this.order != null) {
      data['order'] = this.order.toJson();
    }
    data['tray_code'] = this.trayCode;
    return data;
  }
}

class Order {
  late int id;
  late String orderCode;
  late int companyId;
  late int clientId;
  late String cusOrderNo;
  late int status;
  late List<OrderItem> orderItem;

  Order(
      {required this.id,
      required this.orderCode,
      required this.companyId,
      required this.clientId,
      required this.cusOrderNo,
      required this.status,
      required this.orderItem});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderCode = json['order_code'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    cusOrderNo = json['cus_order_no'];
    status = json['status'];
    if (json['order_item'] != null) {
      orderItem = <OrderItem>[];
      json['order_item'].forEach((v) {
        orderItem.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_code'] = this.orderCode;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['cus_order_no'] = this.cusOrderNo;
    data['status'] = this.status;
    if (this.orderItem != null) {
      data['order_item'] = this.orderItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderItem {
  late int id;
  late int orderId;
  late int skuId;
  late int qty;
  late int status;
  late Sku sku;

  OrderItem(
      {required this.id, required this.orderId, required this.skuId, required this.qty, required this.status, required this.sku});

  OrderItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    status = json['status'];
    sku = (json['sku'] != null ? new Sku.fromJson(json['sku']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.orderId;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    if (this.sku != null) {
      data['sku'] = this.sku.toJson();
    }
    return data;
  }
}

class Sku {
  late int id;
  late String skuCode;
  late String skuName;
  late String barcode1;
  late int companyId;
  late int clientId;
  late int status;
  late String remarks;
  late SkuLocation skuLocation;
  late SkuBatch skuBatch;

  Sku(
      {required this.id,
      required this.skuCode,
      required this.skuName,
      required this.barcode1,
      required this.companyId,
      required this.clientId,
      required this.status,
      required this.remarks,
      required this.skuLocation,
      required this.skuBatch});

  Sku.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuCode = json['sku_code'];
    skuName = json['sku_name'];
    barcode1 = json['barcode_1'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    status = json['status'];
    remarks = json['remarks'];
    skuLocation = (json['skuLocation'] != null
        ? new SkuLocation.fromJson(json['skuLocation'])
        : null)!;
    skuBatch = (json['sku_batch'] != null
        ? new SkuBatch.fromJson(json['sku_batch'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_code'] = this.skuCode;
    data['sku_name'] = this.skuName;
    data['barcode_1'] = this.barcode1;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['status'] = this.status;
    data['remarks'] = this.remarks;
    if (this.skuLocation != null) {
      data['skuLocation'] = this.skuLocation.toJson();
    }
    if (this.skuBatch != null) {
      data['sku_batch'] = this.skuBatch.toJson();
    }
    return data;
  }
}

class SkuLocation {
  late int id;
  late int locationId;
  late int batchId;
  late int qty;
  late int status;

  SkuLocation(
      {required this.id,
      required this.locationId,
      required this.batchId,
      required this.qty,
      required this.status});

  SkuLocation.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    locationId = json['location_id'];
    batchId = json['batch_id'];
    qty = json['qty'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['location_id'] = this.locationId;
    data['batch_id'] = this.batchId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    return data;
  }
}

class SkuBatch {
  late int id;
  late int skuId;
  late int qty;
  late int status;

  SkuBatch(
      {required this.id,
      required this.skuId,
      required this.qty,
      required this.status});

  SkuBatch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    return data;
  }
}
